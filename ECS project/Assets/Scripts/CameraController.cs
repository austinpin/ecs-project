﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float xAxis;
    public float yAxis;
    public float moveSpeed;
    public float rotSpeed;

    public Transform child;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Input.GetAxis("Vertical") * transform.forward * moveSpeed;
        transform.position += Input.GetAxis("Horizontal") * transform.right * moveSpeed;
        transform.position += Input.GetAxis("Ascend") * transform.up * moveSpeed;

        if (Input.GetMouseButton(2))
        {
            yAxis += Input.GetAxis("Mouse X") * rotSpeed;
            xAxis -= Input.GetAxis("Mouse Y") * rotSpeed;

            transform.eulerAngles = new Vector3(0, yAxis, 0);
            child.localEulerAngles = new Vector3(xAxis, 0, 0);
        }
    }
}
