﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Player : IComponentData
{
    public Entity selected;
}
public class PlayerComponent : ComponentDataWrapper<Player> { }
