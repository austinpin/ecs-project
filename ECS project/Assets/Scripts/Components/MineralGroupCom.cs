﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

//stores referance to owned minerals

[InternalBufferCapacity(10)]
public struct MineralArray : IBufferElementData
{
    // These implicit conversions are optional, but can help reduce typing.
    public static implicit operator Entity(MineralArray e) { return e.entity; }
    public static implicit operator MineralArray(Entity e) { return new MineralArray { entity = e }; }

    // Actual value each buffer element will store.
    public Entity entity;
}
