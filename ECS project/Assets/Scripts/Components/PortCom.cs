﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Port : IComponentData
{
    public int x;
    //public Entity input;
    //public Entity output;
}
public class PortCom : ComponentDataWrapper<Port> { }
