﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct WhatImdoing : IComponentData
{
    public WhatImDoingData whatImDoing;
}
public class WhatImDoingCom : ComponentDataWrapper<WhatImdoing> { }

public enum WhatImDoingData : byte
{
    Idle_None,
    FullOfRes,

    TransportingRes,
    TransportingOresForRefine,
    TransportingItemsToStorage,

    MinerShipGoingToMine,
    MinerShipMining,
    MinerShipGoingToUnload,

    RefinerWaitingForRes,
    RefinerRefining,

    ReactorWaitingForRes,
    ReactorMakingPower,

    CrafterIdle,
    CrafterWaitingForRes,
}