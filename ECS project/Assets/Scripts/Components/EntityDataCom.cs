﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

//stores class data for use in jobs to assign data
//jobs cannot be given classes
//this is a workaround,
//but unity developers also call it a solution as having entities to store data for use in game is useful
//sharedcomponentdata accepts class referances which normal componentdata do not

[System.Serializable]
public struct EntityData : ISharedComponentData
{
    public Mesh transportShipMesh;
    public Material transportShipMaterial;

    public Mesh minerShipMesh;
    public Material minerShipMaterial;

    public Mesh MineralMesh;
    public Material MineralMaterial;
}
public class EntityDataCom : SharedComponentDataWrapper<EntityData> { }
