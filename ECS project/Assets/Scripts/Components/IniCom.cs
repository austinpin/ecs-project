﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public enum iniData : byte
{
    Storage,
    Reactor,
    Refiner,
    Crafter,
    Farm,
}

[System.Serializable]
public struct Ini : IComponentData
{
    public iniData data;
}
public class IniCom : ComponentDataWrapper<Ini> { }
