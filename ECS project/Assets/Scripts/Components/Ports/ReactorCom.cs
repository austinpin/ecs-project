﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Reactor : IComponentData
{
    public Entity transportTarget;

    public ushort storedEnergy;
    public ushort energyPerUpdate;
    public float currentFuelLife;

    public float currentTimer;
    public float maxTimer;
}
public class ReactorCom : ComponentDataWrapper<Reactor> { }
