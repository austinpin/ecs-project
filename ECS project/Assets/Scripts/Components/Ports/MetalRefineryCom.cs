﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct MetalRefinery : IComponentData
{
    public byte mode; // 1=normal metal, 2= uranium

    public ushort refineRate;
    public ushort metalStored;

    public Entity storageTarget;

    public float maxTimeDelay;
    public float currentTimeDelay;
}
public class MetalRefineryCom : ComponentDataWrapper<MetalRefinery> { }
