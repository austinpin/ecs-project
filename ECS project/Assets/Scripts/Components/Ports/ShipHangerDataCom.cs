﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

//currently unused to alpha nature of ECS issues :/

    /*
public struct ShipHangerData : IComponentData
{
    public byte inventoryManipulationRate;
    public byte extractFromInventory;

    public float maxTime;
    public float currentTime;
}
public class ShipHangerDataCom : ComponentDataWrapper<ShipHangerData> { }
*/

[InternalBufferCapacity(20)]
public struct ShipHanger : IBufferElementData
{
    // These implicit conversions are optional, but can help reduce typing.
    public static implicit operator Entity(ShipHanger e) { return e.ship; }
    public static implicit operator ShipHanger(Entity ship) { return new ShipHanger { ship=ship }; }

    // Actual value each buffer element will store.
    public Entity ship;
}
