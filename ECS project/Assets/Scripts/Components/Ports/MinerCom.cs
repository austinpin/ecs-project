﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Miner : IComponentData
{
    public byte mode;

    public Entity mineralGroup;
    public Entity transportTarget;

    public Entity sendHanger;
    public Entity minerHanger;
}
public class MinerCom : ComponentDataWrapper<Miner> { }

[System.Serializable]
public struct MinerShip : IComponentData
{
    public byte shouldMine;
    public byte unload;

    public Entity home;
    public Entity mineTarget;

    public float currentTime;
    public float maxTime;
}
