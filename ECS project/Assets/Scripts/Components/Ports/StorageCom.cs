﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Storage : IComponentData
{
    public int x;
}
public class StorageCom : ComponentDataWrapper<Storage> { }
