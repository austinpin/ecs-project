﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Crafter : IComponentData
{
    public Entity transportTarget;
    public Order whatImDoing;
    public byte working;
}
public class CrafterCom : ComponentDataWrapper<Crafter> { }
