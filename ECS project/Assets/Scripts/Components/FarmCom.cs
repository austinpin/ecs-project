﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Farm : IComponentData
{
    public byte productionRate;
    public Entity storageTarget;

    public short waterContent;

    public float maxTime;
    public float currentTime;
}
public class FarmCom : ComponentDataWrapper<Farm> { }
