﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Order
{
    public Entity posTo;
    public Entity InventoryTo;
    public Entity processingEntity; //prevents multiple entities processing the same order
    public byte isTargetInternalInv; //effect of sub inventory entities

    public ItemEnum wantItem;
    public byte dataSet1Full;
    public byte dataSet1Empty;
}

[InternalBufferCapacity(50)]
public struct OrderArray : IBufferElementData
{
    // These implicit conversions are optional, but can help reduce typing.
    public static implicit operator Order(OrderArray e) { return e.order; }
    public static implicit operator OrderArray(Order e) { return new OrderArray { order = e }; }

    // Actual value each buffer element will store.
    public Order order;
}
