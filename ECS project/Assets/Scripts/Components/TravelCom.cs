﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Travel : IComponentData
{
    public float speed;

    public Entity posTarget;
    public Entity inventoryTarget;

    public byte targetInventorySize;
    public float3 targetPos;

    public Entity processedEntity;

    public byte arrive;
    public byte arrivalMode; //1 is terminate, 2 is hover around target
    public byte putIntoInternal; //0 is port //1 is internal //2 is multi inventory
}
public class TravelCom : ComponentDataWrapper<Travel> { }
