﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.UI;

[System.Serializable]
public struct ShowData : ISharedComponentData
{
    public Text whatIAm;
    public Text whatImDoingText;
    public Text data2;

    public Text[] inventoryTexts;
}
public class ShowDataCom : SharedComponentDataWrapper<ShowData> { }
