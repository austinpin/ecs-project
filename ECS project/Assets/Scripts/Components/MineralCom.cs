﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

//can be mined

[System.Serializable]
public struct Mineral : IComponentData
{
    public ItemEnum type;
    public uint2 dataSet1Range;

    //public Entity processor;
}
public class MineralCom : ComponentDataWrapper<Mineral> { }
