﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Owner : ISharedComponentData
{
    public Entity owner;
}
public class OwnerCom : SharedComponentDataWrapper<Owner> { }
