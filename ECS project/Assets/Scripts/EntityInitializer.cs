﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//ensures entities spawn with the correct data
//an effect of the alpha nature of ECS
//for gameobejcts that are also entities
//gameobect stores referance data of its entity for manipulation

public class EntityInitializer : MonoBehaviour
{
    GameObjectEntity self;
    EntityManager em;

    [Header("Inventory sizes")]
    public bool small;
    public bool medium;

    public bool storage;

    [Header("reactor")]
    public bool iniReactor;

    [Header("general")]
    public GameObjectEntity StorageTarget; //storage to store things
    public bool addOrderArray;
    public EntityDataCom data; //the data entity

    [Header("miner")]
    public bool iniMiner;
    public GameObjectEntity transportTarget;
    public GameObjectEntity mineralGroup; //who i mine

    [Header("Refiner")]
    public bool iniRefine;
    public bool iniRefineMetal;
    public bool iniRefineUranium;

    [Header("Farm")]
    public bool iniFarm;

    [Header("crafter")]
    public bool inicraft;

    [Header("Port")]
    public bool iniPort;
    //public bool input;
    //public bool output;

    [Header("MineralGroup")]
    public bool iniMineralGroup;
    public GameObjectEntity[] minerals;
    public byte metalOrUranium;
    public byte ammountToSpawn;
    public Mesh mineralMesh;
    public Material mineralMaterial;

    // Start is called before the first frame update
    void Start()
    {
        self = GetComponent<GameObjectEntity>();
        em = World.Active.GetOrCreateManager<EntityManager>();

        if (small)
        {
            em.AddBuffer<SmallInventoryArray>(self.Entity);
            em.AddComponentData(self.Entity, new InventoryData { sizeCategory=1 , maxLength = 30 });
        }

        if (medium)
        {
            em.AddBuffer<MediumInventoryArray>(self.Entity);
            em.AddComponentData(self.Entity, new InventoryData { sizeCategory = 2 , maxLength = 50});
        }
        if (storage)
        {
            
            em.AddBuffer<MultipleInventoryIndexsArray>(self.Entity);
        }

        if (iniReactor)
        {
            em.AddBuffer<MultipleInventoryIndexsArray>(self.Entity);

            ReactorCom reactorCom = GetComponent<ReactorCom>();
            Reactor reactor = reactorCom.Value;

            reactor.transportTarget = StorageTarget.Entity;
            reactorCom.Value = reactor;
        }
        if (iniFarm)
        {
            em.AddBuffer<MultipleInventoryIndexsArray>(self.Entity);

            FarmCom farmCom = GetComponent<FarmCom>();
            Farm farm = farmCom.Value;

            farm.storageTarget = StorageTarget.Entity;
            farmCom.Value = farm;
        }

        if (iniMiner)
        {
            MinerCom minerCom = GetComponent<MinerCom>();
            Miner m = minerCom.Value;

            m.transportTarget = transportTarget.Entity;
            m.mineralGroup = mineralGroup.Entity;

            m.sendHanger = em.CreateEntity(typeof(ShipHanger));
            Entity e = m.minerHanger = em.CreateEntity(typeof(ShipHanger));

            DynamicBuffer<ShipHanger> mine = em.GetBuffer<ShipHanger>(e);
            DynamicBuffer<MineralArray> targets = em.GetBuffer<MineralArray>(mineralGroup.Entity);
            Entity target = targets[0];
            Entity target2 = targets[1];

            float3 myPos = GetComponent<PositionComponent>().Value.Value;

            //spawn ships
            //an effect of the alpha nature of ECS that i have to spawn them like this
            for (int i=0; i<40; i++)
            {
                Entity ship = em.CreateEntity(Manager.data.minerShip);
                em.SetComponentData(ship, new Position { Value = myPos });
                em.SetSharedComponentData(ship, new MeshInstanceRenderer{ mesh = data.Value.minerShipMesh , material = data.Value.minerShipMaterial });
                em.SetComponentData(ship, new InventoryData { maxLength=10, sizeCategory = 1 });

                targets = em.GetBuffer<MineralArray>(mineralGroup.Entity);
                em.SetComponentData(ship, new Travel { arrivalMode = 2, speed = 6, posTarget = targets[i] });
                em.SetComponentData(ship, new MinerShip { maxTime = 1, shouldMine = 1, home = self.Entity, mineTarget = targets[i] });
            }

            minerCom.Value = m;
        }
        if (iniRefine)
        {
            MetalRefineryCom refineCom = GetComponent<MetalRefineryCom>();
            MetalRefinery refine = refineCom.Value;

            if (iniRefineMetal)
            {
                em.AddBuffer<MultipleInventoryIndexsArray>(self.Entity);
            }
            if (iniRefineUranium)
            {
                em.AddBuffer<MultipleInventoryIndexsArray>(self.Entity);
            }

            refine.storageTarget = StorageTarget.Entity;
            refineCom.Value = refine;
        }
        if (inicraft)
        {
            em.AddBuffer<MultipleInventoryIndexsArray>(self.Entity);
        }
        if (addOrderArray)
        {
            em.AddBuffer<OrderArray>(self.Entity);
        }
        if (iniMineralGroup)
        {
            //a mineral group entities stores a referance to mineral entities which can be mined
            //a miner targets a mineral group
            em.AddBuffer<MineralArray>(self.Entity);
            DynamicBuffer<MineralArray> myMinerals; em.GetBuffer<MineralArray>(self.Entity);
            Unity.Mathematics.Random rand = new Unity.Mathematics.Random(64);
            for (int i=0; i <ammountToSpawn; i++)
            {
                float3 pos = rand.NextFloat3(new float3(-1,-1,-1), new float3(1,1,1));
                pos *= 30;
                pos += GetComponent<PositionComponent>().Value.Value;
                Entity x = em.CreateEntity(Manager.data.mineralVeinArcheType);
                myMinerals = em.GetBuffer<MineralArray>(self.Entity);
                myMinerals.Add(x);
                em.SetComponentData(x, new Position { Value = pos });
                em.SetSharedComponentData(x, new MeshInstanceRenderer { mesh= mineralMesh, material= mineralMaterial });
                if (metalOrUranium==1)
                {
                    em.SetComponentData(x, new Mineral { type=ItemEnum.MetalOre , dataSet1Range = new uint2(10,20) });
                }
                else if(metalOrUranium==2)
                {
                    em.SetComponentData(x, new Mineral { type = ItemEnum.UraniumOre, dataSet1Range = new uint2(5, 10) });
                }
                
            }
        }

        Destroy(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
