﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

//contains information about linked inventory array
//for reasons explained furthur in

[System.Serializable]
public struct InventoryData : IComponentData
{
    public byte sizeCategory;
    public ushort maxLength;
    public ushort currentLength;
}
class InventoryDataComponent : ComponentDataWrapper<InventoryData> { }

public struct IndexInventoryData :IComponentData
{
    public ItemEnum containsIndex;
    public Entity inventoryEntity;
    public byte dataSet1Full;
    public byte dataSet1Empty;

    public byte maxSize;
    public byte currentSize;
    public byte IO; //1-2
}

//these are like arrays but different
//"currently" you cant set their allocation size, but technically they can hold as much as you want
//any item over the limit will use heap memory, and since i want a lot of items, i need to make sure i allocate memory accuratly to minimise heap useage
//these different sizes should help minimize heal allocation
//but create additional coding effort
//heap memory is managed by the system and automatically discarded when its not in use

//the data above has information about its max size and which array to grab;

//as general note
//inventories, when using the buffer, must be transfered from current to new (new is empty so must be filled with current data)
// (the buffer gets a blank template for which the data will be overwritten as , so the current (old) data must be put here)
//can aslo be called initialisation as you cant edit the current inv, and as the new inv is blank
//its needs to be initalised before used,
//this will be a generic structure used in this projects scripting
/*
                DynamicBuffer<SmallInventoryArray> myNewInv = buffer.SetBuffer<SmallInventoryArray>(self[index]);
                DynamicBuffer<SmallInventoryArray> myCurrentInv = allSmallInv[self[index]];

                for (int i = 0; i < myCurrentInv.Length; i++)
                {
                    myNewInv.Add(myCurrentInv[i]);
                }
 */

//change as needed as game adds items
[InternalBufferCapacity(12)]
public struct MultipleInventoryIndexsArray : IBufferElementData
{
    // These implicit conversions are optional, but can help reduce typing.
    public static implicit operator IndexInventoryData(MultipleInventoryIndexsArray e) { return e.indexData; }
    public static implicit operator MultipleInventoryIndexsArray(IndexInventoryData e) { return new MultipleInventoryIndexsArray { indexData = e }; }

    // Actual value each buffer element will store.
    public IndexInventoryData indexData;
}

[InternalBufferCapacity(300)]
public struct SmallInventoryArray : IBufferElementData
{
    // These implicit conversions are optional, but can help reduce typing.
    public static implicit operator Item(SmallInventoryArray e) { return e.item; }
    public static implicit operator SmallInventoryArray(Item e) { return new SmallInventoryArray { item = e }; }

    public static implicit operator SmallInventoryArray(MediumInventoryArray i) { return new SmallInventoryArray { item = i.item }; }

    // Actual value each buffer element will store.
    public Item item;
}

[InternalBufferCapacity(1000)]
public struct MediumInventoryArray : IBufferElementData
{
    // These implicit conversions are optional, but can help reduce typing.
    public static implicit operator Item(MediumInventoryArray e) { return e.item; }
    public static implicit operator MediumInventoryArray(Item e) { return new MediumInventoryArray { item = e }; }

    public static implicit operator MediumInventoryArray(SmallInventoryArray i) { return new MediumInventoryArray { item = i.item }; }

    // Actual value each buffer element will store.
    public Item item;
}
