﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Item
{
    public ItemEnum type;

    public ushort dataSet1; //extra data values tied to item

    public byte size; //for taking up more space
}

public enum ItemEnum : byte
{
    Generic,
    MetalOre,
    MetalRefined,
    UraniumOre,
    UraniumRefined,
    UraniumCell,
    FoodPackage,
    WaterContainer,
    BatteryContainer
}