﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

//what i need to make something

public struct ItemCraftData
{
    public ItemEnum type;
    public byte needed;
}

//what im making

public struct CraftTarget : IComponentData
{
    public ItemEnum targetCraft;
}

//attached array that says all i need to make what i want

[InternalBufferCapacity(10)]
public struct CraftArray : IBufferElementData
{
    // These implicit conversions are optional, but can help reduce typing.
    public static implicit operator ItemCraftData(CraftArray e) { return e.craft; }
    public static implicit operator CraftArray(ItemCraftData e) { return new CraftArray{ craft = e }; }

    // Actual value each buffer element will store.
    public ItemCraftData craft;
}