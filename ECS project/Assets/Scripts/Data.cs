﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;

//contains data for entity-arcehe-types, needed to spawn entities
//and contains crafting recipies

public class Data : MonoBehaviour
{
    public Text countText;
    public Text fps;

    public Text WhatImDoingText1;

    public byte IO;

    public EntityArchetype inventoryEntity;
    EntityManager em;

    public EntityArchetype item;
    public EntityArchetype transportShip;
    public EntityArchetype minerShip;
    public EntityArchetype craftArcheType;

    public EntityArchetype mineralVeinArcheType;

    // Start is called before the first frame update
    void Start()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();
        inventoryEntity = em.CreateArchetype(ComponentType.Create<InventoryData>());

        /*
        item = em.CreateArchetype(
            ComponentType.Create<Item>(),
            ComponentType.Create<Owner>()
            );
            */

        transportShip = em.CreateArchetype(
            ComponentType.Create<Position>(),
            ComponentType.Create<Rotation>(),
            ComponentType.Create<MeshInstanceRenderer>(),
            ComponentType.Create<SmallInventoryArray>(),
            ComponentType.Create<Travel>(),
            ComponentType.Create<WhatImdoing>()
            );

        minerShip = em.CreateArchetype(
            ComponentType.Create<Position>(),
            ComponentType.Create<Rotation>(),
            ComponentType.Create<MeshInstanceRenderer>(),
            ComponentType.Create<SmallInventoryArray>(),
            ComponentType.Create<InventoryData>(),
            ComponentType.Create<Travel>(),
            ComponentType.Create<MinerShip>(),
            ComponentType.Create<WhatImdoing>()
            );

        craftArcheType = em.CreateArchetype(
            ComponentType.Create<CraftTarget>(),
            ComponentType.Create<CraftArray>()
            );

        mineralVeinArcheType = em.CreateArchetype(
            ComponentType.Create<Position>(),
            ComponentType.Create<Rotation>(),
            ComponentType.Create<MeshInstanceRenderer>(),
            ComponentType.Create<Mineral>(),
            ComponentType.Create<WhatImdoing>()
            );

        CreateCraftingRecipies();
    }

    void CreateCraftingRecipies()
    {
        Entity x = em.CreateEntity(craftArcheType);
        em.SetComponentData(x, new CraftTarget {targetCraft = ItemEnum.UraniumCell});
        DynamicBuffer<CraftArray> xa = em.GetBuffer<CraftArray>(x);
        xa.Add(new ItemCraftData {type=ItemEnum.UraniumRefined , needed = 3 });
        xa.Add(new ItemCraftData {type=ItemEnum.MetalRefined , needed = 7 });

        Entity y = em.CreateEntity(craftArcheType);
        em.SetComponentData(y, new CraftTarget { targetCraft = ItemEnum.BatteryContainer });
        DynamicBuffer<CraftArray> ya = em.GetBuffer<CraftArray>(y);
        ya.Add(new ItemCraftData { type = ItemEnum.MetalRefined, needed = 9 });
    }

    public void setIO(int x)
    {
        IO = (byte) x;
    }
}
