﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

//curently does FPS , and stores referance to data
//does nothing else

public class Manager : MonoBehaviour
{
    public static Data data;
    float time=0;
    int frames = 0;

    EntityManager em;

    // Start is called before the first frame update
    void Start()
    {
        data = GetComponent<Data>();
        em = World.Active.GetOrCreateManager<EntityManager>();
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        frames++;

        if (time>=1)
        {
            time -= 1;
            data.fps.text = frames.ToString();
            frames = 0;
        }
    }
}
