﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//tranfers inventories when they arrive

[UpdateAfter(typeof(ArrivalSys))]
public class arrivalBarrier : BarrierSystem { }

[UpdateAfter(typeof(TravelSys))]
public class ArrivalSys : JobComponentSystem
{
    public static JobHandle arrivalHandle;

    struct Miners
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Miner> miner;
        public ComponentDataArray<Position> position;
        //public ComponentDataArray<Rotation> miner;
        public BufferArray<SmallInventoryArray> inv;
    }

    struct TransportShips
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<Travel> travel;
        //[ReadOnly] public BufferArray<SmallInventoryArray> inv; //filter only
    }

    struct DataE
    {
        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;
    }

    //[Inject] Miners _miners;
    [Inject] TransportShips _transport;
    [Inject] DataE _data;

    [Inject] arrivalBarrier barrier;

    [ReadOnly][Inject]BufferFromEntity<SmallInventoryArray> allSmallInv;
    [ReadOnly][Inject]BufferFromEntity<MediumInventoryArray> allMediumInv;
    [ReadOnly] [Inject] BufferFromEntity<OrderArray> allOrders;
    [ReadOnly][Inject]ComponentDataFromEntity<Port> allPort;

    struct TransportArrivalJob : IJobParallelFor
    {
        //primary data set, transport
        public EntityArray transportself;
        [ReadOnly] public ComponentDataArray<Travel> travel;
        //public BufferArray<SmallInventoryArray> transportInv;

        [ReadOnly] public EntityCommandBuffer buffer;

        [ReadOnly]public BufferFromEntity<SmallInventoryArray> allSmallInv;
        [ReadOnly]public BufferFromEntity<MediumInventoryArray> allMediumInv;
        [ReadOnly] public BufferFromEntity<OrderArray> allOrders;
        [ReadOnly] public ComponentDataFromEntity<Port> allPort;

        public void Execute(int index)
        {
            if (travel[index].arrive == 1)
            {
                DynamicBuffer<SmallInventoryArray> myInv;
                myInv = allSmallInv[transportself[index]];

                if (travel[index].arrivalMode == 1)
                {
                    //size 1
                    if (travel[index].targetInventorySize == 1)
                    {
                        //get
                        DynamicBuffer<SmallInventoryArray> newTargetInv = new DynamicBuffer<SmallInventoryArray>();
                        DynamicBuffer<SmallInventoryArray> targetCurrentInv = new DynamicBuffer<SmallInventoryArray>();

                        if (travel[index].putIntoInternal == 1)
                        {
                            newTargetInv = buffer.SetBuffer<SmallInventoryArray>(travel[index].inventoryTarget);
                            targetCurrentInv = allSmallInv[travel[index].inventoryTarget];
                        }
                        else
                        {
                            //Port p = allPort[travel[index].inventoryTarget];
                            //newTargetInv = buffer.SetBuffer<SmallInventoryArray>(p.input);
                            //targetCurrentInv = allSmallInv[p.input];
                        }

                        //add current inventory
                        for (int i = 0; i < targetCurrentInv.Length; i++)
                        {
                            newTargetInv.Add(targetCurrentInv[i]);
                        }

                        //add ships inventory
                        for (int i = 0; i < myInv.Length; i++)
                        {
                            newTargetInv.Add(myInv[i]);
                        }
                    }

                    //size 2
                    if (travel[index].targetInventorySize == 2)
                    {
                        DynamicBuffer<MediumInventoryArray> newTargetInv = new DynamicBuffer<MediumInventoryArray>();
                        DynamicBuffer<MediumInventoryArray> targetCurrentInv = new DynamicBuffer<MediumInventoryArray>();

                        if (travel[index].putIntoInternal == 1)
                        {
                            newTargetInv = buffer.SetBuffer<MediumInventoryArray>(travel[index].inventoryTarget);
                            targetCurrentInv = allMediumInv[travel[index].inventoryTarget];

                        }
                        else
                        {
                            //Port p = allPort[travel[index].inventoryTarget];
                            //newTargetInv = buffer.SetBuffer<MediumInventoryArray>(p.input);
                            //targetCurrentInv = allMediumInv[p.input];
                        }

                        //

                        for (int i = 0; i < targetCurrentInv.Length; i++)
                        {
                            newTargetInv.Add(targetCurrentInv[i]);
                        }

                        for (int i = 0; i < myInv.Length; i++)
                        {
                            newTargetInv.Add(myInv[i]);
                        }
                    }
                    buffer.DestroyEntity(transportself[index]);
                }

                if (travel[index].processedEntity!=Entity.Null)
                {
                    DynamicBuffer<OrderArray> targetCurrentOrders = allOrders[travel[index].posTarget];
                    DynamicBuffer<OrderArray> targetNewOrders = buffer.SetBuffer<OrderArray>(travel[index].posTarget);

                    if (targetCurrentOrders.Length > 0)
                    {
                        for (int i = 0; i < targetCurrentOrders.Length; i++)
                        {
                            targetNewOrders.Add(targetCurrentOrders[i]);
                        }

                        for (int j = 0; j < myInv.Length; j++)
                        {
                            for (int i = 0; i < targetNewOrders.Length; i++)
                            {
                                if (targetNewOrders[i].order.wantItem==myInv[j].item.type)
                                {
                                    if (travel[index].processedEntity==targetNewOrders[i].order.processingEntity)
                                    {
                                        targetNewOrders.RemoveAt(i);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        TransportArrivalJob transportArrivalJob = new TransportArrivalJob
        {
            transportself = _transport.self,
            travel = _transport.travel,
            //transportInv = _transport.inv,

            buffer = barrier.CreateCommandBuffer(),

            allSmallInv =allSmallInv,
            allMediumInv = allMediumInv,
            allPort=allPort,
            allOrders=allOrders,
        };
        //transportArrivalJob.Run(_transport.Length);
        arrivalHandle = transportArrivalJob.Schedule(_transport.Length  ,1, inputDeps);

        return arrivalHandle;
    }
}
