﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//sends items to where they are needed

[UpdateAfter(typeof(PlayerSelectSys))]
public class PlayerSelectBarrier : BarrierSystem { }

[UpdateAfter(typeof(arrivalBarrier))]
public class PlayerSelectSys : JobComponentSystem
{
    public static JobHandle selectHandle;

    struct AllPos
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<Position> pos;
    }

    [Inject] AllPos _allPos;

    [Inject] PlayerSelectBarrier barrier;

    //thanks to (1)
    //https://answers.unity.com/questions/869869/method-of-finding-point-in-3d-space-that-is-exactl.html

    [BurstCompile]
    struct PlayerSelectJob : IJobProcessComponentDataWithEntity<Player>
    {
        //[ReadOnly] public EntityCommandBuffer buffer;
        public float3 rayDirection;
        public float3 rayOrigin;

        public int Length;
        public EntityArray posSelf;
        [ReadOnly] public ComponentDataArray<Position> pos;

        public void Execute(Entity self, int index, ref Player player)
        {
            for (int i=0; i<Length;i++)
            {
                float3 centreToStart = rayOrigin - pos[i].Value;

                //see (1) above for credits and explanation, not sure how this works but its a mathmatical formula for solving for a variable
                //i seen it in maths classes long ago in a different form for solving for x
                float a = math.dot(rayDirection, rayDirection);
                float b = 2 * math.dot(centreToStart, rayDirection); ;
                float c = math.dot(centreToStart, centreToStart) - ((2f * 2) * (2f * 2));

                //if the ray is over the object, then the command effects this current object
                float discriminant = (b * b) - (4 * a * c);

                if (discriminant > 0)
                {
                    player.selected = posSelf[i];
                    break;
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray;
            Vector3 pos = Input.mousePosition;
            ray = Camera.main.ScreenPointToRay(pos);

            PlayerSelectJob playerJob = new PlayerSelectJob
            {
                //buffer = barrier.CreateCommandBuffer(),
                rayOrigin = ray.origin,
                rayDirection = ray.direction,

                Length=_allPos.Length,
                posSelf=_allPos.self,
                pos = _allPos.pos,
            };
            selectHandle = playerJob.Schedule(this, inputDeps);
        }

        return selectHandle;
    }
}
