﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//unused, used to spawn minerals but became obsolete or buggy

[UpdateAfter(typeof(MineralSys))]
public class MineralBarrier : BarrierSystem { }

//[UpdateAfter(typeof(InventorySys))]
public class MineralSys : JobComponentSystem
{
    public static JobHandle mineralHandle;
    public Unity.Mathematics.Random sysRand = new Unity.Mathematics.Random(1);

    struct DataE
    {
        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;
    }

    [Inject] DataE _data;

    [Inject] MineralBarrier barrier;

    [ReadOnly] [Inject] ComponentDataFromEntity<InventoryData> _invT;
    [ReadOnly] [Inject] ComponentDataFromEntity<Port> allPort;

    //change system,
    //mineral spawn have parent owner com
    //find parent mineral and subtract 1 when destroyed
    //is efficient as there are not so many mineral spawns
    //workable and is fast for now, use for speed


    struct MineralJob : IJobProcessComponentDataWithEntity<Mineral, Position>
    {
        public float deltaTime;
        [ReadOnly] public EntityCommandBuffer buffer;
        [ReadOnly] public EntityArchetype mineralRock;

        //other
        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;

        public Unity.Mathematics.Random rand;

        public void Execute(Entity self ,int index,ref Mineral mineral, [ReadOnly] ref Position pos)
        {
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        MineralJob minerJob = new MineralJob
        {
            deltaTime = Time.deltaTime,
            buffer = barrier.CreateCommandBuffer(),
            //mineralRock ,

            dataEntity = _data.dataEntity,
            rand = new Unity.Mathematics.Random(sysRand.NextUInt()),
        };
        //mineralHandle = minerJob.Schedule(this, inputDeps);

        return inputDeps;
    }
}
