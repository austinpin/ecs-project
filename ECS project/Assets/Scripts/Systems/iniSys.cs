﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;


[UpdateBefore(typeof(InventorySys))]
[UpdateBefore(typeof(MineralSys))]
[UpdateBefore(typeof(MinerSys))]
[UpdateBefore(typeof(MinerShipSys))]
[UpdateBefore(typeof(TravelSys))]
[UpdateBefore(typeof(ArrivalSys))]
[UpdateBefore(typeof(MetalRefinerySys))]
[UpdateBefore(typeof(ReactorSys))]
[UpdateBefore(typeof(CraftingSys))]
[UpdateBefore(typeof(StorageSys))]
public class iniSys : ComponentSystem
{

    struct StartData
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<Ini> Inis;
        [ReadOnly] public BufferArray<MultipleInventoryIndexsArray> Multis;
    }

    [Inject] StartData _startData;

    [ReadOnly] [Inject] BufferFromEntity<SmallInventoryArray> allSmallInv;
    [ReadOnly] [Inject] BufferFromEntity<MediumInventoryArray> allMediumInv;
    [ReadOnly] [Inject] BufferFromEntity<MultipleInventoryIndexsArray> allMulti;

    protected override void OnUpdate()
    {
        int length = _startData.Length;
        NativeArray<Ini> inis = new NativeArray<Ini>(length, Allocator.Temp);
        NativeArray<Entity> self = new NativeArray<Entity>(length, Allocator.Temp);

        _startData.self.CopyTo(self, 0);
        _startData.Inis.CopyTo(inis, 0);

        for (int i=0; i<length;i++)
        {
            DynamicBuffer<MultipleInventoryIndexsArray> invs = PostUpdateCommands.SetBuffer<MultipleInventoryIndexsArray>(self[i]);
            if (inis[i].data== iniData.Storage)
            {
                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.BatteryContainer, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 200, IO = 1 , dataSet1Full=1 });
                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.BatteryContainer, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 200, IO = 2 , dataSet1Empty=1 });
                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.FoodPackage, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 200, IO = 1 });
                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.MetalRefined, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 200, IO = 1 });
                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.UraniumCell, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 200, IO = 1 });
                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.UraniumRefined, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 200, IO = 1 });
                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.WaterContainer, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 200, IO = 1 });
            }
            if (inis[i].data == iniData.Reactor)
            {
                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.BatteryContainer, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 30, IO = 1 , dataSet1Empty=1 });
                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.UraniumCell, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 30, IO = 1 });

                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.BatteryContainer, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 30, IO = 2  , dataSet1Full=1});
            }
            if (inis[i].data == iniData.Refiner)
            {
                MetalRefinery r = EntityManager.GetComponentData<MetalRefinery>(self[i]);
                if (r.mode==1)
                {
                    invs.Add(new IndexInventoryData { containsIndex = ItemEnum.MetalOre, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 200, IO = 1 });

                    invs.Add(new IndexInventoryData { containsIndex = ItemEnum.MetalRefined, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 100, IO = 2 });
                }
                if (r.mode == 2)
                {
                    invs.Add(new IndexInventoryData { containsIndex = ItemEnum.UraniumOre, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 200, IO = 1 });

                    invs.Add(new IndexInventoryData { containsIndex = ItemEnum.UraniumRefined, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 100, IO = 2 });
                }
            }
            if (inis[i].data == iniData.Farm)
            {
                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.WaterContainer, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 50, IO = 1 });

                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.FoodPackage, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 50, IO = 2 });
            }
            if (inis[i].data == iniData.Crafter)
            {
                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.Generic, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 50, IO = 1 });

                invs.Add(new IndexInventoryData { containsIndex = ItemEnum.Generic, inventoryEntity = EntityManager.CreateEntity(typeof(SmallInventoryArray)), maxSize = 50, IO = 2 });
            }
            PostUpdateCommands.RemoveComponent<Ini>(self[i]);
        }

        inis.Dispose();
    }
}
