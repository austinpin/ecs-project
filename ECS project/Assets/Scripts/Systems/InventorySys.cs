﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//edit inventory data with length

[UpdateAfter(typeof(arrivalBarrier))]
public class InventorySys : JobComponentSystem
{
    public static JobHandle inventoryHandle;

    struct SmallInv
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<InventoryData> invT;
        [ReadOnly] public BufferArray<SmallInventoryArray> inv;
    }

    struct MediumInv
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<InventoryData> invT;
        [ReadOnly] public BufferArray<MediumInventoryArray> inv;
    }

    struct MultiInv
    {
        public readonly int Length;
        public EntityArray self;
        public BufferArray<MultipleInventoryIndexsArray> invs;
    }

    //[Inject] arrivalBarrier barrier;

    [Inject] MultiInv inv;

    [ReadOnly] [Inject] BufferFromEntity<SmallInventoryArray> allSmallInv;
    [ReadOnly] [Inject] BufferFromEntity<MediumInventoryArray> allMediumInv;
    [ReadOnly] [Inject] BufferFromEntity<MultipleInventoryIndexsArray> allMulti;

    [BurstCompile]
    struct InventoryJob : IJobProcessComponentDataWithEntity<InventoryData>
    {

        [ReadOnly] public BufferFromEntity<SmallInventoryArray> allSmallInv;
        [ReadOnly] public BufferFromEntity<MediumInventoryArray> allMediumInv;
        

        public void Execute(Entity self, int index, ref InventoryData invT)
        {
            if (invT.sizeCategory == 1)
            {
                DynamicBuffer<SmallInventoryArray> smallInv = allSmallInv[self];
                invT.currentLength = (ushort)smallInv.Length;
            }
            else if (invT.sizeCategory == 2)
            {
                DynamicBuffer<MediumInventoryArray> mediumInv = allMediumInv[self];
                invT.currentLength = (ushort)mediumInv.Length;
            }
        }
    }

    [BurstCompile]
    struct MultiInventoryJob : IJobParallelFor
    {
        public int Length;
        public EntityArray self;
        public BufferArray<MultipleInventoryIndexsArray> invs;

        [ReadOnly] public BufferFromEntity<SmallInventoryArray> allSmallInv;
        [ReadOnly] public BufferFromEntity<MediumInventoryArray> allMediumInv;
        //[ReadOnly] public BufferFromEntity<MultipleInventoryIndexsArray> allMulti;

        public void Execute(int index)
        {
            for (int i = 0; i < invs[index].Length; i++)
            {
                DynamicBuffer<SmallInventoryArray> small = allSmallInv[invs[index][i].indexData.inventoryEntity];
                IndexInventoryData x = invs[index][i];
                x.currentSize = (byte)small.Length;
                invs[index].RemoveAt(i);
                invs[index].Insert(i, x);
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        InventoryJob InventoryJob = new InventoryJob
        {
            allSmallInv = allSmallInv,
            allMediumInv = allMediumInv,
        };
        JobHandle newHandle = InventoryJob.Schedule(this, inputDeps);

        MultiInventoryJob job = new MultiInventoryJob
        {
            Length=inv.Length,
            self = inv.self,
            invs =inv.invs,
            
            allSmallInv=allSmallInv,
            allMediumInv=allMediumInv,
            //allMulti=allMulti,
        };
        inventoryHandle = job.Schedule(inv.Length,1, newHandle);

        return inventoryHandle;
    }
}
