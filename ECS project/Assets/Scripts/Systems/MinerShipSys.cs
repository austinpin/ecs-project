﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//goes to target to mine
//when full returns and unloads inventory
//is a looping system where it does one task than a different one when it arrives

[UpdateAfter(typeof(MinerSys))]
public class MinerShipSys : JobComponentSystem
{
    public static JobHandle minerShipHandle;
    private Unity.Mathematics.Random rand = new Unity.Mathematics.Random(5);

    struct MinerShips
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<MinerShip> miner;
        [ReadOnly] public ComponentDataArray<InventoryData> invData;
        public BufferArray<SmallInventoryArray> myInv;
        public ComponentDataArray<Travel> travel;
        public ComponentDataArray<WhatImdoing> whatImDoing;
    }

    struct Miners
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<Miner> miner;
        [ReadOnly] public ComponentDataArray<InventoryData> invData;
        public BufferArray<MediumInventoryArray> myInv;
    }

    [Inject] MinerShips _minerShips;
    [Inject] Miners _miners;

    [ReadOnly] [Inject] ComponentDataFromEntity<Mineral> allMineral;

    [BurstCompile]
    struct MinerShipJob : IJobParallelFor
    {
        public float deltaTime;
        public Unity.Mathematics.Random rand;

        public EntityArray shipSelf;
        public ComponentDataArray<MinerShip> minerShip;
        [ReadOnly] public ComponentDataArray<InventoryData> shipInvData;
        public BufferArray<SmallInventoryArray> mySmallInv;
        public ComponentDataArray<Travel> travel;
        public ComponentDataArray<WhatImdoing> whatImDoing;

        public int Length;
        public EntityArray self;
        //public ComponentDataArray<Miner> miner;
        [ReadOnly] public ComponentDataArray<InventoryData> invData;
        public BufferArray<MediumInventoryArray> myMediumInv;

        //other
        [ReadOnly] public ComponentDataFromEntity<Mineral> allMineral;

        public void Execute(int index)
        {
            if (travel[index].arrive == 1)
            {
                MinerShip currentShip = minerShip[index];
                currentShip.currentTime += deltaTime;

                if (currentShip.currentTime >= currentShip.maxTime)
                {
                    currentShip.currentTime = 0;
                    
                    //mine mode
                    if (currentShip.shouldMine == 1 && travel[index].arrive == 1)
                    {
                        Mineral mineTarget = allMineral[minerShip[index].mineTarget];
                        ushort dataSet1 = (ushort)rand.NextUInt(mineTarget.dataSet1Range.x, mineTarget.dataSet1Range.y);

                        mySmallInv[index].Add(new Item { type = mineTarget.type, dataSet1 = dataSet1 });

                        whatImDoing[index] = new WhatImdoing { whatImDoing = WhatImDoingData.MinerShipMining };

                        //switch mode and return
                        if (shipInvData[index].currentLength >= shipInvData[index].maxLength)
                        {
                            Travel current = travel[index];
                            current.arrive = 0;
                            current.posTarget = minerShip[index].home;
                            travel[index] = current;

                            currentShip.shouldMine = 0;
                            currentShip.unload = 1;
                            whatImDoing[index] = new WhatImdoing { whatImDoing = WhatImDoingData.MinerShipGoingToUnload };
                        }
                    }

                    //unload mode
                    if (currentShip.shouldMine == 0 && travel[index].arrive == 1)
                    {
                        //switch mode and return
                        if (currentShip.unload==2)
                        {

                        }

                        if (shipInvData[index].currentLength == 0)
                        {
                            Travel current = travel[index];
                            current.arrive = 0;
                            current.posTarget = currentShip.mineTarget;
                            travel[index] = current;

                            currentShip.shouldMine = 1;
                            whatImDoing[index] = new WhatImdoing { whatImDoing = WhatImDoingData.MinerShipGoingToMine };
                        }
                    }
                }

                minerShip[index] = currentShip;
            }
        }
    }

    [BurstCompile]
    struct MinerJob : IJobParallelFor
    {
        public float deltaTime;
        
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<InventoryData> invData;
        public BufferArray<MediumInventoryArray> myMediumInv;

        public int Length;
        [ReadOnly] public ComponentDataArray<MinerShip> minerShip;
        [NativeDisableParallelForRestriction] public BufferArray<SmallInventoryArray> mySmallInv;
        [ReadOnly] public ComponentDataArray<Travel> travel;

        public void Execute(int index)
        {
            for (int i = 0; i < Length; i++)
            {
                MinerShip currentShip = minerShip[i];
                if (currentShip.home == self[index] && currentShip.shouldMine == 0 && travel[i].arrive == 1 )
                {
                    for (int j = 0; j < mySmallInv[i].Length; j++)
                    {
                        if (invData[index].currentLength < invData[index].maxLength)
                        {
                            myMediumInv[index].Add(mySmallInv[i][j]);
                            mySmallInv[i].RemoveAt(j);
                            j--;
                            continue;
                        }
                        break;
                    }
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        MinerShipJob minerShipJob = new MinerShipJob
        {
            deltaTime = Time.deltaTime,
            rand = new Unity.Mathematics.Random(rand.NextUInt()),

            shipSelf = _minerShips.self,
            minerShip = _minerShips.miner,
            shipInvData = _minerShips.invData,
            mySmallInv = _minerShips.myInv,
            travel = _minerShips.travel,
            whatImDoing = _minerShips.whatImDoing,

            Length = _miners.Length,
            myMediumInv = _miners.myInv,
            invData = _miners.invData,
            self = _miners.self,

            allMineral = allMineral,
        };
        JobHandle newHandle = minerShipJob.Schedule(_minerShips.Length, 1, inputDeps);

        MinerJob minerJob = new MinerJob
        {
            deltaTime = Time.deltaTime,

            self = _miners.self,
            invData = _miners.invData,
            myMediumInv = _miners.myInv,

            Length = _minerShips.Length,
            minerShip = _minerShips.miner,
            mySmallInv = _minerShips.myInv,
            travel = _minerShips.travel,
        };
        minerShipHandle = minerJob.Schedule(_miners.Length, 1, newHandle);

        return minerShipHandle;
    }
}