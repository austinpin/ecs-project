﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//makes things and send them

[UpdateAfter(typeof(CraftingSys))]
public class CraftingBarrier : BarrierSystem { }

[UpdateAfter(typeof(arrivalBarrier))]
[UpdateAfter(typeof(StorageBarrier))]
public class CraftingSys : JobComponentSystem
{
    public static JobHandle craftingHandle;

    struct DataE
    {
        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;
    }

    struct CraftData
    {
        public readonly int Length;
        [ReadOnly] public ComponentDataArray<CraftTarget> craftTargets;
        [ReadOnly] public BufferArray<CraftArray> craftArrays;
    }

    struct Orders
    {
        public readonly int Length;
        public EntityArray orderSelf;
        [ReadOnly] public BufferArray<OrderArray> orderArrays;
    }

    [Inject] DataE _data;
    [Inject] CraftData _craftData;
    [Inject] Orders _orders;

    [Inject] CraftingBarrier barrier;

    [ReadOnly] [Inject] BufferFromEntity<SmallInventoryArray> allSmallInv;
    [ReadOnly] [Inject] BufferFromEntity<MediumInventoryArray> allMediumInv;
    [ReadOnly] [Inject] BufferFromEntity<MultipleInventoryIndexsArray> allMulti;
    [ReadOnly] [Inject] BufferFromEntity<OrderArray> allOrders;
    [ReadOnly] [Inject] ComponentDataFromEntity<InventoryData> _invT;

    struct CraftingJob : IJobProcessComponentDataWithEntity<Port, Crafter, WhatImdoing, Position>
    {
        public float deltaTime;
        [ReadOnly] public EntityCommandBuffer buffer;
        public EntityArchetype transportShip;

        public int craftsLength;
        [ReadOnly] public ComponentDataArray<CraftTarget> craftTargets;
        [ReadOnly] public BufferArray<CraftArray> craftArrays;

        public int ordersLength;
        public EntityArray orderSelf;
        [ReadOnly] public BufferArray<OrderArray> orderArrays;

        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;

        [ReadOnly] public BufferFromEntity<SmallInventoryArray> allSmallInv;
        [ReadOnly] public BufferFromEntity<MediumInventoryArray> allMediumInv;
        [ReadOnly] public BufferFromEntity<MultipleInventoryIndexsArray> allMulti;
        [ReadOnly] public BufferFromEntity<OrderArray> allOrders;
        [ReadOnly] public ComponentDataFromEntity<InventoryData> _invT;

        public void Execute(Entity self, int index, ref Port port, ref Crafter crafter, ref WhatImdoing whatImDoing , [ReadOnly] ref Position pos)
        {
            //set up input and output

            DynamicBuffer<SmallInventoryArray> myCurrentInputInv = new DynamicBuffer<SmallInventoryArray>();
            DynamicBuffer<SmallInventoryArray> myNewInputInv = new DynamicBuffer<SmallInventoryArray>();

            DynamicBuffer<SmallInventoryArray> myCurrentOutputInv = new DynamicBuffer<SmallInventoryArray>();
            DynamicBuffer<SmallInventoryArray> myNewOutputInv = new DynamicBuffer<SmallInventoryArray>();

            int myOutputFree = 0;
            Entity input = Entity.Null;
            Entity output = Entity.Null;

            DynamicBuffer<MultipleInventoryIndexsArray> myinvs = allMulti[self];
            for (int i = 0; i < myinvs.Length; i++)
            {
                if (myinvs[i].indexData.IO == 1)
                {
                    myCurrentInputInv = allSmallInv[myinvs[i].indexData.inventoryEntity];
                    myNewInputInv = buffer.SetBuffer<SmallInventoryArray>(myinvs[i].indexData.inventoryEntity);
                    input = myinvs[i].indexData.inventoryEntity;
                }
                if (myinvs[i].indexData.IO == 2)
                {
                    myCurrentOutputInv = allSmallInv[myinvs[i].indexData.inventoryEntity];
                    myNewOutputInv = buffer.SetBuffer<SmallInventoryArray>(myinvs[i].indexData.inventoryEntity);
                    myOutputFree = myinvs[i].indexData.maxSize - myinvs[i].indexData.currentSize;
                    output = myinvs[i].indexData.inventoryEntity;
                }
            }

            for (int i = 0; i < myCurrentInputInv.Length; i++)
            {
                myNewInputInv.Add(myCurrentInputInv[i]);
            }

            for (int i = 0; i < myCurrentOutputInv.Length; i++)
            {
                myNewOutputInv.Add(myCurrentOutputInv[i]);
            }

            //inialise end

            //if i dont have a job get a job
            if (crafter.working == 0)
            {
                whatImDoing.whatImDoing = WhatImDoingData.CrafterIdle;
                byte end = 0;
                //go theough all entities with order
                for (int a = 0; a < ordersLength; a++)
                {
                    //check all orders
                    for (int i = 0; i < orderArrays[a].Length; i++)
                    {
                        //if someone else if working on it skip
                        if (orderArrays[a][i].order.processingEntity!=Entity.Null)
                        {
                            continue;
                        }
                        //check all crafting recipies and compare
                        for (int b = 0; b < craftsLength; b++)
                        {
                            //if its creatable
                            if (craftTargets[b].targetCraft == orderArrays[a][i].order.wantItem)
                            {
                                //copy work data
                                //modify original order to state im working on it
                                crafter.whatImDoing = orderArrays[a][i].order;
                                DynamicBuffer<OrderArray> targetNewOrders = buffer.SetBuffer<OrderArray>(orderSelf[a]);

                                for (int l = 0; l < orderArrays[a].Length; l++)
                                {
                                    targetNewOrders.Add(orderArrays[a][l].order);
                                }

                                Order working = targetNewOrders[i].order;
                                targetNewOrders.RemoveAt(i);
                                working.processingEntity = self;
                                targetNewOrders.Insert(i,working);

                                end = 1;
                                crafter.working = 1;
                                break;
                            }
                        }
                        if (end == 1)
                        {
                            break;
                        }
                    }
                    if (end == 1)
                    {
                        break;
                    }
                }
            }
            //if i have a job
            if (crafter.working==1)
            {
                whatImDoing.whatImDoing = WhatImDoingData.CrafterWaitingForRes;
                //see if i have enough resources to create the item
                //find crafting recipie
                for (int b = 0; b < craftsLength; b++)
                {
                    if (craftTargets[b].targetCraft == crafter.whatImDoing.wantItem)
                    {
                        byte numberOfValid = 0; //crafting is doable if, i have all the items of an array index
                        for (int j = 0; j < craftArrays[b].Length; j++)
                        {
                            //assign current item of crafting recipie
                            //see if i have the items
                            ItemCraftData data = craftArrays[b][j].craft; 
                            byte currentNeeded = 0;

                            //check inventory
                            for (int k = 0; k < myNewInputInv.Length; k++)
                            {
                                if (myNewInputInv[k].item.type == data.type)
                                {
                                    currentNeeded++;
                                    if (currentNeeded >= data.needed)
                                    {
                                        //validate
                                        numberOfValid++;
                                        break;
                                    }
                                }
                            }
                            if (currentNeeded < data.needed)
                            {
                                //if i do not have enough resources, create an order
                                //check previous orders to prevent duplicate creation
                                DynamicBuffer<OrderArray> myCurrentOrders = allOrders[self];
                                DynamicBuffer<OrderArray> myNewOrders = buffer.SetBuffer<OrderArray>(self);

                                for (int i = 0; i < myCurrentOrders.Length; i++)
                                {
                                    myNewOrders.Add(myCurrentOrders[i]);
                                }

                                //compare orders
                                for (int i = 0; i < myNewOrders.Length; i++)
                                {
                                    if (myNewOrders[i].order.wantItem == data.type)
                                    {
                                        currentNeeded++;
                                        if (currentNeeded >= data.needed)
                                        {
                                            break;
                                        }
                                    }
                                }
                                //create new orders as needed
                                byte diff = (byte) (data.needed - currentNeeded);
                                for (int l=0; l<diff; l++)
                                {
                                    myNewOrders.Add(new Order { posTo = self , InventoryTo = input , wantItem = data.type , isTargetInternalInv=1 });
                                }
                            }
                        }
                        //if i have all items
                        if (numberOfValid==craftArrays[b].Length)
                        {
                            //once i have enough resources, create the item and send it off
                            //myCurrentOutputInv.Add(new Item { type = crafter.whatImDoing.wantItem });
                            buffer.CreateEntity(transportShip);
                            buffer.SetComponent(new Position { Value = pos.Value });
                            buffer.SetSharedComponent(new MeshInstanceRenderer { mesh = dataEntity[0].transportShipMesh, material = dataEntity[0].transportShipMaterial });
                            //transfer inventory
                            DynamicBuffer<SmallInventoryArray> shipsBuffer = buffer.SetBuffer<SmallInventoryArray>();
                            shipsBuffer.Add(new Item { type = crafter.whatImDoing.wantItem });
                            buffer.SetComponent(new Travel { posTarget = crafter.whatImDoing.posTo, inventoryTarget = crafter.whatImDoing.InventoryTo ,
                                speed = 12, targetInventorySize = 1, putIntoInternal = crafter.whatImDoing.isTargetInternalInv,
                                arrivalMode = 1 , processedEntity=self });
                            buffer.SetComponent(new WhatImdoing { whatImDoing = WhatImDoingData.TransportingRes });
                            crafter.working = 0;
                            crafter.whatImDoing.wantItem = ItemEnum.Generic;

                            //remove used items
                            for (int j = 0; j < craftArrays[b].Length; j++)
                            {
                                ItemCraftData data = craftArrays[b][j].craft;
                                byte currentNeeded = 0;

                                for (int k = 0; k < myNewInputInv.Length; k++)
                                {
                                    if (myNewInputInv[k].item.type == data.type)
                                    {
                                        currentNeeded++;
                                        myNewInputInv.RemoveAt(k);
                                        k--;
                                        if (currentNeeded >= data.needed)
                                        {
                                            //dont delete too much
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        CraftingJob craftingJob = new CraftingJob
        {
            deltaTime = Time.deltaTime,
            buffer = barrier.CreateCommandBuffer(),
            transportShip = Manager.data.transportShip,

            craftsLength= _craftData.Length,
            craftTargets=_craftData.craftTargets,
            craftArrays = _craftData.craftArrays,

            ordersLength = _orders.Length,
            orderSelf=_orders.orderSelf,
            orderArrays = _orders.orderArrays,

            dataEntity= _data.dataEntity,

            allSmallInv = allSmallInv,
            allMediumInv = allMediumInv,
            allMulti=allMulti,
            allOrders=allOrders,
            _invT = _invT,
        };
        craftingHandle = craftingJob.Schedule(this, inputDeps);

        return craftingHandle;
    }
}
