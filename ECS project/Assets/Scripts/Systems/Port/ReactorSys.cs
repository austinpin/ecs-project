﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//makes power and orders items
//puts power into batteries

[UpdateAfter(typeof(ReactorSys))]
public class ReactorBarrier : BarrierSystem { }

[UpdateAfter(typeof(arrivalBarrier))]
public class ReactorSys : JobComponentSystem
{
    public static JobHandle reactorHandle;

    struct DataE
    {
        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;
    }

    [Inject] DataE _data;

    [Inject] ReactorBarrier barrier;

    [ReadOnly] [Inject] BufferFromEntity<SmallInventoryArray> allSmallInv;
    [ReadOnly] [Inject] BufferFromEntity<MediumInventoryArray> allMediumInv;
    [ReadOnly] [Inject] BufferFromEntity<MultipleInventoryIndexsArray> allMulti;
    [ReadOnly] [Inject] BufferFromEntity<OrderArray> allOrders;
    [ReadOnly] [Inject] ComponentDataFromEntity<InventoryData> _invT;

    struct ReactorJob : IJobProcessComponentDataWithEntity<Port,Reactor, WhatImdoing , Position>
    {
        public float deltaTime;
        [ReadOnly] public EntityCommandBuffer buffer;
        public EntityArchetype transportShip;

        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;

        [ReadOnly] public BufferFromEntity<SmallInventoryArray> allSmallInv;
        [ReadOnly] public BufferFromEntity<MediumInventoryArray> allMediumInv;
        [ReadOnly] public BufferFromEntity<MultipleInventoryIndexsArray> allMulti;
        [ReadOnly] public BufferFromEntity<OrderArray> allOrders;
        [ReadOnly] public ComponentDataFromEntity<InventoryData> _invT;

        public void Execute(Entity self, int index, ref Port port, ref Reactor reactor, ref WhatImdoing whatImDoing, [ReadOnly] ref Position pos)
        {
            //verify timer
            reactor.currentTimer += deltaTime;
            if (reactor.currentTimer >= reactor.maxTimer)
            {
                reactor.currentTimer = 0;

                //set up input and output

                DynamicBuffer<SmallInventoryArray> myCurrentUraniumCellInputInv = new DynamicBuffer<SmallInventoryArray>();
                DynamicBuffer<SmallInventoryArray> myNewUraniumCellInputInv = new DynamicBuffer<SmallInventoryArray>();

                DynamicBuffer<SmallInventoryArray> myCurrentBatteryInputInv = new DynamicBuffer<SmallInventoryArray>();
                DynamicBuffer<SmallInventoryArray> myNewBatteryInputInv = new DynamicBuffer<SmallInventoryArray>();

                DynamicBuffer<SmallInventoryArray> myCurrentOutputInv = new DynamicBuffer<SmallInventoryArray>();
                DynamicBuffer<SmallInventoryArray> myNewOutputInv = new DynamicBuffer<SmallInventoryArray>();

                int myOutputFree = 0;
                Entity inputUranium = Entity.Null;
                Entity inputBattery = Entity.Null;

                DynamicBuffer<MultipleInventoryIndexsArray> myinvs = allMulti[self];
                for (int i = 0; i < myinvs.Length; i++)
                {
                    if (myinvs[i].indexData.IO == 1 && myinvs[i].indexData.containsIndex==ItemEnum.UraniumCell)
                    {
                        myCurrentUraniumCellInputInv = allSmallInv[myinvs[i].indexData.inventoryEntity];
                        myNewUraniumCellInputInv = buffer.SetBuffer<SmallInventoryArray>(myinvs[i].indexData.inventoryEntity);
                        inputUranium = myinvs[i].indexData.inventoryEntity;
                    }
                    if (myinvs[i].indexData.IO == 1 && myinvs[i].indexData.containsIndex == ItemEnum.BatteryContainer)
                    {
                        myCurrentBatteryInputInv = allSmallInv[myinvs[i].indexData.inventoryEntity];
                        myNewBatteryInputInv = buffer.SetBuffer<SmallInventoryArray>(myinvs[i].indexData.inventoryEntity);
                        inputBattery = myinvs[i].indexData.inventoryEntity;
                    }
                    if (myinvs[i].indexData.IO == 2)
                    {
                        myCurrentOutputInv = allSmallInv[myinvs[i].indexData.inventoryEntity];
                        myNewOutputInv = buffer.SetBuffer<SmallInventoryArray>(myinvs[i].indexData.inventoryEntity);
                        myOutputFree = myinvs[i].indexData.maxSize - myinvs[i].indexData.currentSize;
                    }
                }

                int numberOfFuelCells=0;
                int numberOfBattery=0;

                whatImDoing.whatImDoing = WhatImDoingData.ReactorWaitingForRes;

                //add current inv to new inv, but with a twist
                //add fuel to this reactor using the fuel cells in input
                for (int i = 0; i < myCurrentUraniumCellInputInv.Length; i++)
                {
                    if(reactor.currentFuelLife < 499)
                    {
                        if (myCurrentUraniumCellInputInv[i].item.type==ItemEnum.UraniumCell)
                        {
                            reactor.currentFuelLife += 500;
                            continue; //"remove item by ignoing its add mode"
                        }
                    }
                    if (myCurrentUraniumCellInputInv[i].item.type == ItemEnum.UraniumCell)
                    {
                        numberOfFuelCells++;
                    }
                    myNewUraniumCellInputInv.Add(myCurrentUraniumCellInputInv[i]);
                }

                //do same for battery
                for (int i = 0; i < myCurrentBatteryInputInv.Length; i++)
                {
                    if (myCurrentBatteryInputInv[i].item.type == ItemEnum.BatteryContainer)
                    {
                        numberOfBattery++;
                    }
                    myNewBatteryInputInv.Add(myCurrentBatteryInputInv[i]);
                }


                //check all pending orders to prevent duplicate order creation
                //add this number to number of fuel cells i have
                DynamicBuffer<OrderArray> myCurrentOrders = allOrders[self];
                DynamicBuffer<OrderArray> myNewOrders = buffer.SetBuffer<OrderArray>(self);

                for (int i=0; i<myCurrentOrders.Length;i++)
                {
                    myNewOrders.Add(myCurrentOrders[i]);
                }

                for(int i=0; i<myNewOrders.Length;i++)
                {
                    if (myNewOrders[i].order.wantItem==ItemEnum.UraniumCell)
                    {
                        numberOfFuelCells++;
                    }
                    if (myNewOrders[i].order.wantItem == ItemEnum.BatteryContainer)
                    {
                        numberOfBattery++;
                    }
                }

                //dont have enough fuel? create order
                int neededCells = (5 - numberOfFuelCells);
                int neededBattery = (5 - numberOfBattery);

                if (neededCells>0 )
                {
                    for (int i=0; i<neededCells; i++)
                    {
                        myNewOrders.Add(new Order { posTo = self , InventoryTo = inputUranium , wantItem =  ItemEnum.UraniumCell , isTargetInternalInv=1 });
                    }
                }
                if (neededBattery>0)
                {
                    for (int i = 0; i < neededBattery; i++)
                    {
                        myNewOrders.Add(new Order { posTo = self, InventoryTo = inputBattery, wantItem = ItemEnum.BatteryContainer, isTargetInternalInv = 1, dataSet1Empty = 1 });
                    }
                }

                //initialise inv
                for (int i = 0; i < myCurrentOutputInv.Length; i++)
                {
                    myNewOutputInv.Add(myCurrentOutputInv[i]);
                }

                if (reactor.currentFuelLife>0) {
                    whatImDoing.whatImDoing = WhatImDoingData.ReactorMakingPower;

                    //reduce life add fuel
                    reactor.currentFuelLife -= reactor.maxTimer;
                    reactor.storedEnergy += reactor.energyPerUpdate;
                    //limit stored energy
                    if (reactor.storedEnergy>50000)
                    {
                        reactor.storedEnergy = 50000;
                    }

                    //put fuel into battery
                    //dont charge if output is full
                    if (myOutputFree>0) {
                        for (int i = 0; i < myNewBatteryInputInv.Length; i++)
                        {
                            if (myNewBatteryInputInv[i].item.type == ItemEnum.BatteryContainer)
                            {
                                ushort num = (ushort)(10000 - myNewBatteryInputInv[i].item.dataSet1); //differance
                                Item battery = myNewBatteryInputInv[i].item;

                                //make full, transfer to output
                                if (myNewBatteryInputInv[i].item.dataSet1 + reactor.storedEnergy >= 10000)
                                {
                                    battery.dataSet1 += num;
                                    reactor.storedEnergy -= num;

                                    myNewBatteryInputInv[i] = new SmallInventoryArray { item = battery };
                                    myNewOutputInv.Add(myNewBatteryInputInv[i]);
                                    myNewBatteryInputInv.RemoveAt(i);
                                }
                                else //cannot charge full so do thiss
                                {
                                    battery.dataSet1 += reactor.storedEnergy;
                                    reactor.storedEnergy = 0;
                                    myNewBatteryInputInv[i] = new SmallInventoryArray { item = battery };
                                }
                                break;
                            }
                        }
                    }

                    int targetFree = 0;
                    Entity target = Entity.Null;

                    DynamicBuffer<MultipleInventoryIndexsArray> targetMulti = allMulti[reactor.transportTarget];
                    for (int i = 0; i < targetMulti.Length; i++)
                    {
                        if (targetMulti[i].indexData.containsIndex == ItemEnum.BatteryContainer && targetMulti[i].indexData.dataSet1Full==1)
                        {
                            targetFree = targetMulti[i].indexData.maxSize - targetMulti[i].indexData.currentSize;
                            target = targetMulti[i].indexData.inventoryEntity;
                            break;
                        }
                    }

                    /*
                    //send dont overfill
                    InventoryData tag = _invT[refine.storageTarget];
                    targetFree = tag.maxLength - tag.currentLength;
                    */

                    if (targetFree >= 7 && myNewOutputInv.Length>0)
                    {
                        //send to destination
                        buffer.CreateEntity(transportShip);
                        buffer.SetComponent(new Position { Value = pos.Value });
                        buffer.SetSharedComponent(new MeshInstanceRenderer { mesh = dataEntity[0].transportShipMesh, material = dataEntity[0].transportShipMaterial });
                        buffer.SetComponent(new Travel { posTarget = reactor.transportTarget, inventoryTarget = target, speed = 12, targetInventorySize = 1, putIntoInternal = 1, arrivalMode = 1 });
                        buffer.SetComponent(new WhatImdoing { whatImDoing = WhatImDoingData.TransportingItemsToStorage });

                        //transfer inventory
                        DynamicBuffer<SmallInventoryArray> shipsBuffer = buffer.SetBuffer<SmallInventoryArray>();
                        int length1 = myNewOutputInv.Length;
                        for (int i = 0; i < length1; i++)
                        {
                            if (myNewOutputInv[i].item.type == ItemEnum.BatteryContainer)
                            {
                                shipsBuffer.Add(myNewOutputInv[i]);
                                myNewOutputInv.RemoveAt(i);
                                length1--;
                                i--;
                            }
                        }
                    }
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        ReactorJob reactorJob = new ReactorJob
        {
            deltaTime = Time.deltaTime,
            buffer = barrier.CreateCommandBuffer(),
            transportShip=Manager.data.transportShip,

            dataEntity=_data.dataEntity,

            allSmallInv = allSmallInv,
            allMediumInv =allMediumInv,
            allMulti=allMulti,
            allOrders=allOrders,
            _invT=_invT,
        };
        reactorHandle = reactorJob.Schedule(this, inputDeps);

        return reactorHandle;
    }
}
