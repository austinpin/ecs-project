﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//sends items to where they are needed

[UpdateAfter(typeof(StorageSys))]
public class StorageBarrier : BarrierSystem { }

[UpdateAfter(typeof(arrivalBarrier))]
public class StorageSys : JobComponentSystem
{
    public static JobHandle storageHandle;

    struct DataE
    {
        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;
    }

    struct CraftData
    {
        public readonly int Length;
        [ReadOnly] public ComponentDataArray<CraftTarget> craftTargets;
        [ReadOnly] public BufferArray<CraftArray> craftArrays;
    }

    struct Orders
    {
        public readonly int Length;
        public EntityArray orderSelf;
        public BufferArray<OrderArray> orderArrays;
    }

    [Inject] DataE _data;
    [Inject] CraftData _craftData;
    [Inject] Orders _orders;

    [Inject] StorageBarrier barrier;

    [ReadOnly] [Inject] BufferFromEntity<SmallInventoryArray> allSmallInv;
    [ReadOnly] [Inject] BufferFromEntity<MediumInventoryArray> allMediumInv;
    [ReadOnly] [Inject] BufferFromEntity<MultipleInventoryIndexsArray> allMulti;
    [ReadOnly] [Inject] BufferFromEntity<OrderArray> allOrders;
    [ReadOnly] [Inject] ComponentDataFromEntity<InventoryData> _invT;

    struct StorageJob : IJobProcessComponentDataWithEntity<Port, Storage, Position>
    {
        [ReadOnly] public EntityCommandBuffer buffer;
        public EntityArchetype transportShip;

        public int ordersLength;
        public EntityArray orderSelf;
        public BufferArray<OrderArray> orderArrays;

        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;

        [ReadOnly] public BufferFromEntity<SmallInventoryArray> allSmallInv;
        [ReadOnly] public BufferFromEntity<MediumInventoryArray> allMediumInv;
        [ReadOnly] public BufferFromEntity<MultipleInventoryIndexsArray> allMulti;
        //[ReadOnly] public BufferFromEntity<OrderArray> allOrders;
        [ReadOnly] public ComponentDataFromEntity<InventoryData> _invT;

        public void Execute(Entity self, int index, ref Port port, ref Storage storage, [ReadOnly] ref Position pos)
        {

            DynamicBuffer<MultipleInventoryIndexsArray> myInvs = allMulti[self];

            byte check=0;

            for (int a = 0; a < ordersLength; a++)
            {
                //ship going to this target overall inventory (set later)
                DynamicBuffer<SmallInventoryArray> shipsBuffer = new DynamicBuffer<SmallInventoryArray>();

                //check if this entity needs at least 1 item
                for (int i = 0; i < orderArrays[a].Length; i++)
                {
                    for (int j = 0; j < myInvs.Length; j++)
                    {
                        if (myInvs[j].indexData.currentSize>0)
                        {
                            if (orderArrays[a][i].order.processingEntity == Entity.Null)
                            {
                                if (orderArrays[a][i].order.wantItem == myInvs[j].indexData.containsIndex)
                                {
                                    if (myInvs[j].indexData.dataSet1Full == 1 && orderArrays[a][i].order.dataSet1Full==1)
                                    {
                                        check = 1;
                                        //if we can we create a ship, later we will add the items so as to create only 1 ship
                                        //and send all items it needs
                                        buffer.CreateEntity(transportShip);
                                        buffer.SetComponent(new Position { Value = pos.Value });
                                        buffer.SetSharedComponent(new MeshInstanceRenderer { mesh = dataEntity[0].transportShipMesh, material = dataEntity[0].transportShipMaterial });
                                        shipsBuffer = buffer.SetBuffer<SmallInventoryArray>();
                                        buffer.SetComponent(new Travel
                                        {
                                            posTarget = orderSelf[a],
                                            inventoryTarget = orderArrays[a][i].order.InventoryTo,
                                            speed = 12,
                                            targetInventorySize = 1,
                                            putIntoInternal = orderArrays[a][i].order.isTargetInternalInv,
                                            arrivalMode = 1,
                                            processedEntity = self
                                        });
                                        buffer.SetComponent(new WhatImdoing { whatImDoing = WhatImDoingData.TransportingRes });
                                        break;
                                    }
                                    else if (myInvs[j].indexData.dataSet1Empty == 1 && orderArrays[a][i].order.dataSet1Empty==1)
                                    {
                                        check = 1;
                                        //if we can we create a ship, later we will add the items so as to create only 1 ship
                                        //and send all items it needs
                                        buffer.CreateEntity(transportShip);
                                        buffer.SetComponent(new Position { Value = pos.Value });
                                        buffer.SetSharedComponent(new MeshInstanceRenderer { mesh = dataEntity[0].transportShipMesh, material = dataEntity[0].transportShipMaterial });
                                        shipsBuffer = buffer.SetBuffer<SmallInventoryArray>();
                                        buffer.SetComponent(new Travel
                                        {
                                            posTarget = orderSelf[a],
                                            inventoryTarget = orderArrays[a][i].order.InventoryTo,
                                            speed = 12,
                                            targetInventorySize = 1,
                                            putIntoInternal = orderArrays[a][i].order.isTargetInternalInv,
                                            arrivalMode = 1,
                                            processedEntity = self
                                        });
                                        buffer.SetComponent(new WhatImdoing { whatImDoing = WhatImDoingData.TransportingRes });
                                        break;
                                    }
                                    else if(myInvs[j].indexData.dataSet1Empty == 0 && myInvs[j].indexData.dataSet1Full==0)
                                    {
                                        check = 1;
                                        //if we can we create a ship, later we will add the items so as to create only 1 ship
                                        //and send all items it needs
                                        buffer.CreateEntity(transportShip);
                                        buffer.SetComponent(new Position { Value = pos.Value });
                                        buffer.SetSharedComponent(new MeshInstanceRenderer { mesh = dataEntity[0].transportShipMesh, material = dataEntity[0].transportShipMaterial });
                                        shipsBuffer = buffer.SetBuffer<SmallInventoryArray>();
                                        buffer.SetComponent(new Travel
                                        {
                                            posTarget = orderSelf[a],
                                            inventoryTarget = orderArrays[a][i].order.InventoryTo,
                                            speed = 12,
                                            targetInventorySize = 1,
                                            putIntoInternal = orderArrays[a][i].order.isTargetInternalInv,
                                            arrivalMode = 1,
                                            processedEntity = self
                                        });
                                        buffer.SetComponent(new WhatImdoing { whatImDoing = WhatImDoingData.TransportingRes });
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (check == 1)
                    {
                        break;
                    }
                }
                
                if (check==1)
                {
                    //repeat but this time add the item to the ship buffer
                    for (int j = 0; j < myInvs.Length; j++)
                    {

                        DynamicBuffer<SmallInventoryArray> currentInv = allSmallInv[myInvs[j].indexData.inventoryEntity];
                        DynamicBuffer<SmallInventoryArray> newInv = buffer.SetBuffer<SmallInventoryArray>(myInvs[j].indexData.inventoryEntity);

                        for (int i = 0; i < currentInv.Length; i++)
                        {
                            newInv.Add(currentInv[i]);
                        }

                        for (int i = 0; i < orderArrays[a].Length; i++)
                        {
                            if (orderArrays[a][i].order.processingEntity==Entity.Null)
                            {
                                if (orderArrays[a][i].order.wantItem == myInvs[j].indexData.containsIndex)
                                {
                                    if (newInv.Length > 0)
                                    {
                                        shipsBuffer.Add(newInv[0].item);
                                        newInv.RemoveAt(0);
                                        Order o = orderArrays[a][i];
                                        o.processingEntity = self;
                                        orderArrays[a].RemoveAt(i);
                                        orderArrays[a].Insert(i, o);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        ReactorSys.reactorHandle.Complete();
        StorageJob storageJob = new StorageJob
        {
            buffer = barrier.CreateCommandBuffer(),
            transportShip = Manager.data.transportShip,

            ordersLength = _orders.Length,
            orderSelf = _orders.orderSelf,
            orderArrays = _orders.orderArrays,

            dataEntity = _data.dataEntity,

            allSmallInv = allSmallInv,
            allMediumInv = allMediumInv,
            allMulti=allMulti,
            //allOrders = allOrders,
            _invT = _invT,
        };
        //storageHandle = storageJob.Schedule(this, inputDeps);
        storageJob.Run(this);

        return storageHandle;
    }
}
