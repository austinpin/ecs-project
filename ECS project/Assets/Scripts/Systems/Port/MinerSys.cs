﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//sends inventory to target currently

//[UpdateAfter(typeof(MinerSys))]
//public class MinerBarrier: BarrierSystem { }

//[UpdateAfter(typeof(InventorySys))]
public class MinerSys : ComponentSystem
{
    public static JobHandle minerHandle;
    //private Unity.Mathematics.Random rand = new Unity.Mathematics.Random(5);

    struct Miners
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Miner> miner;
        [ReadOnly] public ComponentDataArray<Position> position;
        public BufferArray<MediumInventoryArray> inv;
        [ReadOnly] public ComponentDataArray<InventoryData> invData;
    }

    struct DataE
    {
        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;
    }

    [Inject] Miners _miners;
    [Inject] DataE _data;

    //[Inject] MinerBarrier barrier;

    [ReadOnly] [Inject] ComponentDataFromEntity<InventoryData> _invT;
    [ReadOnly] [Inject] BufferFromEntity<MultipleInventoryIndexsArray> allMulti;
    [ReadOnly] [Inject] ComponentDataFromEntity<Port> allPort;

    /*
    struct MinerJob : IJobParallelFor
    {
        public float deltaTime;
        [ReadOnly] public EntityCommandBuffer buffer;
        [ReadOnly] public EntityArchetype transportShip;

        //public int minerLength;
        public EntityArray minerSelf;
        public ComponentDataArray<Miner> miner;
        [ReadOnly] public ComponentDataArray<Position> position;
        public BufferArray<MediumInventoryArray> inv;
        [ReadOnly] public ComponentDataArray<InventoryData> invT;

        //other
        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;
        //other
        [ReadOnly] public ComponentDataFromEntity<InventoryData> _invT;
        [ReadOnly] public BufferFromEntity<MultipleInventoryIndexsArray> allMulti;
        [ReadOnly] public ComponentDataFromEntity<Port> allPort;

        public void Execute(int index)
        {
            Miner currentMiner = miner[index];

            //Entity hanger = miner[index].minerHanger;

            DynamicBuffer<MultipleInventoryIndexsArray> targetInvs = allMulti[currentMiner.transportTarget];
            Entity target = Entity.Null;
            int targetFree=0;

            for (int i=0; i< targetInvs.Length ;i++)
            {
                if (targetInvs[i].indexData.IO==1)
                {
                    target = targetInvs[i].indexData.inventoryEntity;
                    targetFree = targetInvs[i].indexData.maxSize - targetInvs[i].indexData.currentSize;
                }
            }

            if (inv[index].Length > 50 && targetFree > 50)
            {
                buffer.CreateEntity(transportShip);
                buffer.SetComponent(new Position { Value = position[index].Value });
                buffer.SetSharedComponent(new MeshInstanceRenderer { mesh = dataEntity[0].transportShipMesh, material = dataEntity[0].transportShipMaterial });
                //transfer inventory
                DynamicBuffer<SmallInventoryArray> shipsBuffer = buffer.SetBuffer<SmallInventoryArray>();

                for (int x = 0; x < inv[index].Length; x++)
                {
                    shipsBuffer.Add(inv[index][x]);
                }

                inv[index].Clear();
                buffer.SetComponent(new Travel { posTarget = currentMiner.transportTarget , inventoryTarget = target , speed = 8, targetInventorySize = 1 , putIntoInternal=1 , arrivalMode = 1 });
                buffer.SetComponent(new WhatImdoing { whatImDoing = WhatImDoingData.TransportingOresForRefine });
            }
            miner[index] = currentMiner;
        }
    }
    */

    protected override void OnUpdate()
    {
        /*
        MinerJob minerJob = new MinerJob
        {
            deltaTime = Time.deltaTime,
            buffer = barrier.CreateCommandBuffer(),
            transportShip = Manager.data.transportShip,

            minerSelf = _miners.self,
            miner = _miners.miner,
            position = _miners.position,
            inv = _miners.inv,
            invT = _miners.invData,

            dataEntity = _data.dataEntity,
            _invT = _invT,
            allMulti=allMulti,
            allPort = allPort,
        };
        minerHandle = minerJob.Schedule(_miners.Length,1, inputDeps);
        */

        //EntityCommandBuffer buffer = barrier.CreateCommandBuffer();

        for (int index=0;index<_miners.Length;index++)
        {
            Miner currentMiner = _miners.miner[index];

            //Entity hanger = miner[index].minerHanger;

            DynamicBuffer<MultipleInventoryIndexsArray> targetInvs = allMulti[currentMiner.transportTarget];
            Entity target = Entity.Null;
            int targetFree = 0;

            for (int i = 0; i < targetInvs.Length; i++)
            {
                if (targetInvs[i].indexData.IO == 1)
                {
                    target = targetInvs[i].indexData.inventoryEntity;
                    targetFree = targetInvs[i].indexData.maxSize - targetInvs[i].indexData.currentSize;
                }
            }

            if (_miners.inv[index].Length > 50 && targetFree > 50)
            {
                PostUpdateCommands.CreateEntity(Manager.data.transportShip);
                PostUpdateCommands.SetComponent(new Position { Value = _miners.position[index].Value });
                PostUpdateCommands.SetSharedComponent(new MeshInstanceRenderer { mesh = _data.dataEntity[0].transportShipMesh, material = _data.dataEntity[0].transportShipMaterial });
                //transfer inventory
                DynamicBuffer<SmallInventoryArray> shipsBuffer = PostUpdateCommands.SetBuffer<SmallInventoryArray>();

                for (int x = 0; x < _miners.inv[index].Length; x++)
                {
                    shipsBuffer.Add(_miners.inv[index][x]);
                }

                _miners.inv[index].Clear();
                PostUpdateCommands.SetComponent(new Travel { posTarget = currentMiner.transportTarget, inventoryTarget = target, speed = 8, targetInventorySize = 1, putIntoInternal = 1, arrivalMode = 1 });
                PostUpdateCommands.SetComponent(new WhatImdoing { whatImDoing = WhatImDoingData.TransportingOresForRefine });
            }
            _miners.miner[index] = currentMiner;
        }

        //return minerHandle;
    }
}
