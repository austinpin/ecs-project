﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//converts ore into its refined counterpart

[UpdateAfter(typeof(MetalRefinerySys))]
public class MetalRefineryBarrier : BarrierSystem { }

[UpdateAfter(typeof(arrivalBarrier))]
public class MetalRefinerySys : JobComponentSystem
{
    public static JobHandle MetalRefineryHandle;

    struct DataE
    {
        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;
    }

    [Inject] DataE _data;
    [Inject] MetalRefineryBarrier barrier;

    [ReadOnly][Inject]BufferFromEntity<SmallInventoryArray> allSmallInv;
    [ReadOnly][Inject]BufferFromEntity<MediumInventoryArray> allMediumInv;
    [ReadOnly][Inject]BufferFromEntity<MultipleInventoryIndexsArray> allMulti;
    [ReadOnly] [Inject] ComponentDataFromEntity<InventoryData> _invT;

    struct PortRefineJob : IJobProcessComponentDataWithEntity<Port,MetalRefinery, Position, WhatImdoing>
    {
        [ReadOnly] public BufferFromEntity<SmallInventoryArray> allSmallInv;
        [ReadOnly] public BufferFromEntity<MediumInventoryArray> allMediumInv;
        [ReadOnly] public BufferFromEntity<MultipleInventoryIndexsArray> allMulti;
        [ReadOnly] public ComponentDataFromEntity<InventoryData> _invT;

        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;

        [ReadOnly] public EntityCommandBuffer buffer;
        [ReadOnly] public EntityArchetype transportShip;
        public float deltaTime;

        public void Execute(Entity self, int index, ref Port port, ref MetalRefinery refine , [ReadOnly] ref Position pos , ref WhatImdoing whatImDoing)
        {
            refine.currentTimeDelay += deltaTime;
            if (refine.currentTimeDelay >= refine.maxTimeDelay)
            {
                refine.currentTimeDelay = 0;

                //set up input and output

                DynamicBuffer<SmallInventoryArray> myCurrentInputInv = new DynamicBuffer<SmallInventoryArray>();
                DynamicBuffer<SmallInventoryArray> myNewInputInv = new DynamicBuffer<SmallInventoryArray>();

                DynamicBuffer<SmallInventoryArray> myCurrentOutputInv = new DynamicBuffer<SmallInventoryArray>();
                DynamicBuffer<SmallInventoryArray> myNewOutputInv = new DynamicBuffer<SmallInventoryArray>();

                int myOutputFree = 0;

                DynamicBuffer<MultipleInventoryIndexsArray> myinvs = allMulti[self];
                for (int i=0; i<myinvs.Length ; i++)
                {
                    if (myinvs[i].indexData.IO == 1)
                    {
                        myCurrentInputInv = allSmallInv[myinvs[i].indexData.inventoryEntity];
                        myNewInputInv = buffer.SetBuffer<SmallInventoryArray>(myinvs[i].indexData.inventoryEntity);

                    }
                    if (myinvs[i].indexData.IO == 2)
                    { 
                        myCurrentOutputInv = allSmallInv[myinvs[i].indexData.inventoryEntity];
                        myNewOutputInv = buffer.SetBuffer<SmallInventoryArray>(myinvs[i].indexData.inventoryEntity);
                        myOutputFree = myinvs[i].indexData.maxSize - myinvs[i].indexData.currentSize;
                    }
                }

                for (int i=0; i<myCurrentInputInv.Length;i++)
                {
                    myNewInputInv.Add(myCurrentInputInv[i]);
                }

                for (int i = 0; i < myCurrentOutputInv.Length; i++)
                {
                    myNewOutputInv.Add(myCurrentOutputInv[i]);
                }

                whatImDoing.whatImDoing = WhatImDoingData.RefinerWaitingForRes;

                ushort refineUsage = 0;
                
                int length = myNewInputInv.Length;
                for (int i = 0; i < length; i++)
                {
                    if (refineUsage > refine.refineRate)
                    {
                        break;
                    }

                    whatImDoing.whatImDoing = WhatImDoingData.RefinerRefining;

                    //if we are processing metal ore and have metal ore
                    if (myNewInputInv[i].item.type == ItemEnum.MetalOre && refine.mode==1)
                    {
                        //if processing the metal ore wont overfill metal storage
                        if (refine.metalStored+myNewInputInv[i].item.dataSet1<=1000)
                        {
                            refine.metalStored += myNewInputInv[i].item.dataSet1;
                            refineUsage++;
                            myNewInputInv.RemoveAt(i);
                            length--;
                            i--;
                            continue;
                        }
                        //if it will put us over storage, skip this section (ignore other metal ores even if we can be filled)
                        break;
                    }

                    //uranium mode
                    if (myNewInputInv[i].item.type == ItemEnum.UraniumOre && refine.mode==2)
                    {
                        if (refine.metalStored + myNewInputInv[i].item.dataSet1 <= 1000)
                        {
                            refine.metalStored += myNewInputInv[i].item.dataSet1;
                            refineUsage++;
                            myNewInputInv.RemoveAt(i);
                            length--;
                            i--;
                            continue;
                        }
                        break;
                    }
                }

                //actually create the refined metal but dont overfill

                for (int i=0;i< myOutputFree; i++)
                {
                    if (refine.metalStored>=100)
                    {
                        if (refine.mode == 1)
                        {
                            myNewOutputInv.Add(new MediumInventoryArray { item = new Item { type = ItemEnum.MetalRefined } });
                        }
                        if (refine.mode == 2)
                        {
                            myNewOutputInv.Add(new MediumInventoryArray { item = new Item { type = ItemEnum.UraniumRefined } });
                        }
                        refine.metalStored -= 100;
                        continue;
                    }
                    break;
                }

                if (myOutputFree==0)
                {
                    whatImDoing.whatImDoing = WhatImDoingData.FullOfRes;
                }

                //if i have things to send then send
                int numberOfRefinedMetal=0;

                for (int i=0; i<myNewOutputInv.Length;i++)
                {
                    if (myNewOutputInv[i].item.type==ItemEnum.MetalRefined || myNewOutputInv[i].item.type == ItemEnum.UraniumRefined)
                    {
                        numberOfRefinedMetal++;
                    }
                }

                int targetFree = 0;
                Entity target = Entity.Null;

                DynamicBuffer<MultipleInventoryIndexsArray> targetMulti = allMulti[refine.storageTarget];
                for (int i = 0; i < targetMulti.Length; i++)
                {
                    if (targetMulti[i].indexData.containsIndex == ItemEnum.UraniumRefined && refine.mode==2)
                    {
                        targetFree = targetMulti[i].indexData.maxSize - targetMulti[i].indexData.currentSize;
                        target = targetMulti[i].indexData.inventoryEntity;
                        break;
                    }
                    if (targetMulti[i].indexData.containsIndex == ItemEnum.MetalRefined && refine.mode == 1)
                    {
                        targetFree = targetMulti[i].indexData.maxSize - targetMulti[i].indexData.currentSize;
                        target = targetMulti[i].indexData.inventoryEntity;
                        break;
                    }
                }

                /*
                //send dont overfill
                InventoryData tag = _invT[refine.storageTarget];
                targetFree = tag.maxLength - tag.currentLength;
                */

                if (numberOfRefinedMetal > 7 && targetFree >= 7)
                {
                    //send to destination
                    buffer.CreateEntity(transportShip);
                    buffer.SetComponent(new Position { Value = pos.Value });
                    buffer.SetSharedComponent(new MeshInstanceRenderer { mesh = dataEntity[0].transportShipMesh, material = dataEntity[0].transportShipMaterial });
                    buffer.SetComponent(new Travel { posTarget = refine.storageTarget , inventoryTarget = target , speed = 12, targetInventorySize = 1, putIntoInternal = 1, arrivalMode = 1 });
                    buffer.SetComponent(new WhatImdoing { whatImDoing = WhatImDoingData.TransportingItemsToStorage });

                    //transfer inventory
                    DynamicBuffer<SmallInventoryArray> shipsBuffer = buffer.SetBuffer<SmallInventoryArray>();
                    int length1 = myNewOutputInv.Length;
                    for (int i = 0; i < length1; i++)
                    {
                        if (myNewOutputInv[i].item.type == ItemEnum.MetalRefined || myNewOutputInv[i].item.type == ItemEnum.UraniumRefined)
                        {
                            shipsBuffer.Add(myNewOutputInv[i]);
                            myNewOutputInv.RemoveAt(i);
                            length1--;
                            i--;
                        }
                    }
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        PortRefineJob portRefineJob = new PortRefineJob
        {
            allSmallInv = allSmallInv,
            allMediumInv = allMediumInv,
            allMulti=allMulti,
            _invT=_invT,

            dataEntity=_data.dataEntity,

            buffer = barrier.CreateCommandBuffer(),
            deltaTime = Time.deltaTime,
            transportShip=Manager.data.transportShip,
        };
        MetalRefineryHandle = portRefineJob.Schedule(this, inputDeps);

        return MetalRefineryHandle;
    }
}
