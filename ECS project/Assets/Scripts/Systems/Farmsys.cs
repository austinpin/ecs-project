﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//sends inventory to target currently

//[UpdateAfter(typeof(MinerSys))]
public class FarmBarrier : BarrierSystem { }

//[UpdateAfter(typeof(InventorySys))]
public class FarmSys : JobComponentSystem
{
    public static JobHandle farmHandle;
    //private Unity.Mathematics.Random rand = new Unity.Mathematics.Random(5);

    struct DataE
    {
        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;
    }

    [Inject] DataE _data;

    [Inject] FarmBarrier barrier;

    [ReadOnly] [Inject] BufferFromEntity<SmallInventoryArray> allSmallInv;
    [ReadOnly] [Inject] ComponentDataFromEntity<InventoryData> _invT;
    [ReadOnly] [Inject] BufferFromEntity<MultipleInventoryIndexsArray> allMulti;

    struct FarmJob : IJobProcessComponentDataWithEntity<Port, Farm, Position, WhatImdoing>
    {
        public float deltaTime;
        [ReadOnly] public EntityCommandBuffer buffer;
        [ReadOnly] public EntityArchetype transportShip;

        //other
        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;
        //other
        [ReadOnly] public BufferFromEntity<SmallInventoryArray> allSmallInv;
        [ReadOnly] public ComponentDataFromEntity<InventoryData> _invT;
        [ReadOnly] public BufferFromEntity<MultipleInventoryIndexsArray> allMulti;

        public void Execute(Entity self, int index, ref Port port, ref Farm farm, [ReadOnly] ref Position pos, ref WhatImdoing whatImDoing)
        {
            farm.currentTime += deltaTime;
            if (farm.currentTime>farm.maxTime)
            {
                farm.currentTime = 0;
                DynamicBuffer<MultipleInventoryIndexsArray> myinvs = allMulti[self];

                DynamicBuffer<SmallInventoryArray> myCurrentOutputInv = new DynamicBuffer<SmallInventoryArray>();
                DynamicBuffer<SmallInventoryArray> myNewOutputInv = new DynamicBuffer<SmallInventoryArray>();

                int myOutputFree = 0;

                for (int i = 0; i < myinvs.Length; i++)
                {
                    if (myinvs[i].indexData.IO == 1)
                    {
                        //myCurrentInputInv = allSmallInv[myinvs[i].indexData.inventoryEntity];
                        //myNewInputInv = buffer.SetBuffer<SmallInventoryArray>(myinvs[i].indexData.inventoryEntity);

                    }
                    if (myinvs[i].indexData.IO == 2)
                    {
                        myCurrentOutputInv = allSmallInv[myinvs[i].indexData.inventoryEntity];
                        myNewOutputInv = buffer.SetBuffer<SmallInventoryArray>(myinvs[i].indexData.inventoryEntity);
                        myOutputFree = myinvs[i].indexData.maxSize - myinvs[i].indexData.currentSize;
                    }
                }

                for (int i=0; i<farm.productionRate;i++)
                {
                    if (myOutputFree>0)
                    {
                        myNewOutputInv.Add(new Item { type = ItemEnum.FoodPackage });
                        myOutputFree--;
                    }
                }

                int targetFree = 0;
                Entity target = Entity.Null;

                DynamicBuffer<MultipleInventoryIndexsArray> targetMulti = allMulti[farm.storageTarget];
                for (int i = 0; i < targetMulti.Length; i++)
                {
                    if (targetMulti[i].indexData.containsIndex == ItemEnum.FoodPackage)
                    {
                        targetFree = targetMulti[i].indexData.maxSize - targetMulti[i].indexData.currentSize;
                        target = targetMulti[i].indexData.inventoryEntity;
                        break;
                    }
                }

                if (targetFree >= 1)
                {
                    //send to destination
                    buffer.CreateEntity(transportShip);
                    buffer.SetComponent(new Position { Value = pos.Value });
                    buffer.SetSharedComponent(new MeshInstanceRenderer { mesh = dataEntity[0].transportShipMesh, material = dataEntity[0].transportShipMaterial });
                    buffer.SetComponent(new Travel { posTarget = farm.storageTarget, inventoryTarget = target, speed = 12, targetInventorySize = 1, putIntoInternal = 1, arrivalMode = 1 });
                    buffer.SetComponent(new WhatImdoing { whatImDoing = WhatImDoingData.TransportingItemsToStorage });

                    //transfer inventory
                    DynamicBuffer<SmallInventoryArray> shipsBuffer = buffer.SetBuffer<SmallInventoryArray>();
                    int length1 = myNewOutputInv.Length;
                    for (int i = 0; i < length1; i++)
                    {
                        if (myNewOutputInv[i].item.type == ItemEnum.FoodPackage)
                        {
                            shipsBuffer.Add(myNewOutputInv[i]);
                            myNewOutputInv.RemoveAt(i);
                            length1--;
                            i--;
                        }
                    }
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        FarmJob farmJob = new FarmJob
        {
            deltaTime = Time.deltaTime,
            buffer = barrier.CreateCommandBuffer(),
            transportShip = Manager.data.transportShip,

            dataEntity = _data.dataEntity,

            allSmallInv=allSmallInv,
            _invT = _invT,
            allMulti = allMulti,
        };
        farmHandle = farmJob.Schedule(this, inputDeps);

        return farmHandle;
    }
}
