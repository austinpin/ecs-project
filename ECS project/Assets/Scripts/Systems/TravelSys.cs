﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//makes things move

//[UpdateAfter(typeof(MinerBarrier))]
[UpdateAfter(typeof(MinerSys))]
public class TravelSys : JobComponentSystem
{
    public static JobHandle travelHandle;

    struct DataE
    {
        [ReadOnly] public SharedComponentDataArray<EntityData> dataEntity;
    }

    [Inject] DataE _data;

    [ReadOnly] [Inject] ComponentDataFromEntity<Position> allPositions;

    [BurstCompile]
    struct SetTargetJob : IJobProcessComponentData<Travel>
    {
        [ReadOnly]
        public ComponentDataFromEntity<Position> allPositions;

        public void Execute(ref Travel travel)
        {
            if (travel.posTarget!=Entity.Null)
            {
                float3 targetPos = allPositions[travel.posTarget].Value;
                travel.targetPos = targetPos;
            }
        }
    }

    [BurstCompile]
    struct TravelJob : IJobProcessComponentData<Position,Travel>
    {
        public float deltaTime;

        public void Execute(ref Position pos, ref Travel travel)
        {
            if (travel.arrive==0)
            {
                float3 targetPos = travel.targetPos;
                float3 toTarget = targetPos - pos.Value;
                float3 direction = math.normalize(toTarget);

                float3 moveDistance = direction * travel.speed * deltaTime;

                if (math.lengthsq(toTarget) <= math.lengthsq((moveDistance*2)))
                {
                    //pos.Value = targetPos + new float3(1, 1, 1);
                    travel.arrive = 1;
                }
                else
                {
                    float3 newPos = pos.Value + moveDistance;
                    pos.Value = newPos;
                }
            }
        }
    }


    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        //incase it moves grab its new position
        //we have no referances here
        SetTargetJob setTargetJob = new SetTargetJob
        {
            allPositions=allPositions,
        };
        JobHandle newHandle1 = setTargetJob.Schedule(this, inputDeps);

        TravelJob minerJob = new TravelJob
        {
            deltaTime = Time.deltaTime,
        };
        travelHandle = minerJob.Schedule(this, newHandle1);

        return travelHandle;
    }
}
