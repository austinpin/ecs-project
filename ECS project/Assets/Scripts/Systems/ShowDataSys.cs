﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;
using UnityEngine.UI;

[UpdateAfter(typeof(arrivalBarrier))]
public class ShowDataSys : ComponentSystem
{

    struct ShowData_
    {
        [ReadOnly] public SharedComponentDataArray<ShowData> showData;
    }

    struct PlayerData_
    {
        public readonly int Length;
        [ReadOnly] public ComponentDataArray<Player> player;
    }

    [Inject] ShowData_ _data;
    [Inject] PlayerData_ _player;

    [ReadOnly] [Inject] BufferFromEntity<SmallInventoryArray> allSmallInv;
    [ReadOnly] [Inject] BufferFromEntity<MediumInventoryArray> allMediumInv;
    [ReadOnly] [Inject] BufferFromEntity<MultipleInventoryIndexsArray> allMulti;
    [ReadOnly] [Inject] BufferFromEntity<OrderArray> allOrders;
    [ReadOnly] [Inject] ComponentDataFromEntity<InventoryData> _invT;
    [ReadOnly] [Inject] ComponentDataFromEntity<WhatImdoing> whatImDoing;

    protected override void OnUpdate()
    {
        Entity selected = _player.player[0].selected;
        Text nameText = _data.showData[0].whatIAm;
        Text whatImDoingText = _data.showData[0].whatImDoingText;
        Text secondDebugText = _data.showData[0].data2;

        nameText.text = "";
        whatImDoingText.text = "";
        secondDebugText.text = "";

        Text[] inventoryDebugArray = _data.showData[0].inventoryTexts;
        int[] ammountByType = new int[inventoryDebugArray.Length];

        if (!EntityManager.Exists(selected))
        {
            nameText.text = "dead entity";
            whatImDoingText.text = "Completed service?";
            secondDebugText.text = "Inventory before death" ;
            return;
        }
        if (!EntityManager.HasComponent<WhatImdoing>(selected))
        {
            nameText.text = "untracked entity";
            whatImDoingText.text = "";
            secondDebugText.text = "";
            for (int i = 0; i < inventoryDebugArray.Length; i++)
            {
                inventoryDebugArray[i].text = "n/a";
            }
            return;
        }
        WhatImdoing doing = whatImDoing[selected];

        //NAMES
        if (EntityManager.HasComponent<MinerShip>(selected))
        {
            nameText.text = "minership";
        }
        else if (!EntityManager.HasComponent<MinerShip>(selected))
        {
            if (EntityManager.HasComponent<Travel>(selected))
            {
                nameText.text = "transport ship";
            }
        }
        if (EntityManager.HasComponent<MetalRefinery>(selected))
        {
            nameText.text = "refinery";
        }
        else if (EntityManager.HasComponent<Storage>(selected))
        {
            nameText.text = "storage";
        }
        else if (EntityManager.HasComponent<Crafter>(selected))
        {
            nameText.text = "crafter";
        }
        else if (EntityManager.HasComponent<Reactor>(selected))
        {
            nameText.text = "reactor";
            secondDebugText.text = EntityManager.GetComponentData<Reactor>(selected).storedEnergy.ToString();
        }
        else if (EntityManager.HasComponent<Miner>(selected))
        {
            nameText.text = "miner";
        }
        else if (EntityManager.HasComponent<Mineral>(selected))
        {
            nameText.text = "mineral";
            Mineral m = EntityManager.GetComponentData<Mineral>(selected);
            if (m.type == ItemEnum.MetalOre)
            {
                whatImDoingText.text = "i give metal ore";
            }else if (m.type == ItemEnum.UraniumOre)
            {
                whatImDoingText.text = "i give uranium ore";
            }

            secondDebugText.text = m.dataSet1Range.ToString();
            
            return;
        }

        //WHAT THEY ARE DOING
        if (doing.whatImDoing == WhatImDoingData.Idle_None)
        {
            whatImDoingText.text = "I am doing nothing";
        }else if (doing.whatImDoing == WhatImDoingData.CrafterWaitingForRes)
        {
            whatImDoingText.text = "waiting for resources";
        }
        else if (doing.whatImDoing == WhatImDoingData.FullOfRes)
        {
            whatImDoingText.text = "inventory full, target also full";
        }
        else if (doing.whatImDoing == WhatImDoingData.Idle_None)
        {
            whatImDoingText.text = "doing nothing";
        }
        else if (doing.whatImDoing == WhatImDoingData.MinerShipGoingToMine)
        {
            whatImDoingText.text = "going to mine";
        }
        else if (doing.whatImDoing == WhatImDoingData.MinerShipGoingToUnload)
        {
            whatImDoingText.text = "inventory full, going to unload";
        }
        else if (doing.whatImDoing == WhatImDoingData.MinerShipMining)
        {
            whatImDoingText.text = "currently mining resoruces";
        }
        else if (doing.whatImDoing == WhatImDoingData.ReactorMakingPower)
        {
            whatImDoingText.text = "creating power";
        }
        else if (doing.whatImDoing == WhatImDoingData.ReactorWaitingForRes)
        {
            whatImDoingText.text = "waiting for resources to function";
        }
        else if (doing.whatImDoing == WhatImDoingData.RefinerRefining)
        {
            whatImDoingText.text = "refining ores";
        }
        else if (doing.whatImDoing == WhatImDoingData.RefinerWaitingForRes)
        {
            whatImDoingText.text = "waiting for ores";
        }
        else if (doing.whatImDoing == WhatImDoingData.TransportingRes)
        {
            whatImDoingText.text = "transporting resources to target";
        }
        else if (doing.whatImDoing == WhatImDoingData.TransportingOresForRefine)
        {
            whatImDoingText.text = "trasnsporting ores for refining";
        }
        else if (doing.whatImDoing == WhatImDoingData.TransportingItemsToStorage)
        {
            whatImDoingText.text = "transporting items to storage";
        }

        //INVENTORY
        if (EntityManager.HasComponent<SmallInventoryArray>(selected))
        {
            DynamicBuffer<SmallInventoryArray> small = EntityManager.GetBuffer<SmallInventoryArray>(selected);
            for (int i=0; i<small.Length; i++)
            {
                int current = ammountByType[(int)small[i].item.type];
                current++;
                ammountByType[(int)small[i].item.type] = current;
            }
            for (int i=0; i <inventoryDebugArray.Length; i++)
            {
                inventoryDebugArray[i].text = ammountByType[i].ToString();
            }
        }else if (EntityManager.HasComponent<MultipleInventoryIndexsArray>(selected))
        {
            DynamicBuffer<MultipleInventoryIndexsArray> multi = EntityManager.GetBuffer<MultipleInventoryIndexsArray>(selected);

            //debugs both input and output
            //change code to allow choice

            for (int i=0; i<multi.Length; i++)
            {
                if (multi[i].indexData.IO == Manager.data.IO) {
                    int current = ammountByType[(int)multi[i].indexData.containsIndex];
                    current = multi[i].indexData.currentSize;
                    ammountByType[(int)multi[i].indexData.containsIndex] = current;
                }
            }
            for (int i = 0; i < inventoryDebugArray.Length; i++)
            {
                inventoryDebugArray[i].text = ammountByType[i].ToString();
            }
        }else if (EntityManager.HasComponent<MediumInventoryArray>(selected))
        {
            DynamicBuffer<MediumInventoryArray> small = EntityManager.GetBuffer<MediumInventoryArray>(selected);
            for (int i = 0; i < small.Length; i++)
            {
                int current = ammountByType[(int)small[i].item.type];
                current++;
                ammountByType[(int)small[i].item.type] = current;
            }
            for (int i = 0; i < inventoryDebugArray.Length; i++)
            {
                inventoryDebugArray[i].text = ammountByType[i].ToString();
            }
        }
        else
        {
            for (int i = 0; i < inventoryDebugArray.Length; i++)
            {
                inventoryDebugArray[i].text = "n/a";
            }
        }
    }
}
